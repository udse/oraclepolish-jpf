package testassist.listener;

import java.util.HashSet;
import java.util.Set;

import testassist.bytecode.ret.NATIVERETURN;
import testassist.mark.Marks;
import testassist.util.AttrHelper;
import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.JVMNativeStackFrame;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;
import gov.nasa.jpf.vm.VM;

public class NativeMethodListener extends MyListener {

	public NativeMethodListener(TestInfo ti) {
		super(ti);
	}

	public static void setDeepAttrs(ElementInfo ei, MJIEnv env, Marks ms, Set<ElementInfo> visited) {
		//				if (ms != null) ms = ms.filterAMarks();
		if (null == ei || visited.contains(ei)) return;

		ei = ei.getModifiableInstance();

		visited.add(ei);

		ei.setObjectAttr(ms);

		if (ei.isArray()) {
			for (int i = 0; i < ei.arrayLength(); i++) {
				ei.setElementAttr(i, ms);
			}

			if (ei.isReferenceArray()) {
				for (int ref : ei.asReferenceArray()) {
					ElementInfo element = env.getModifiableElementInfo(ref);
					setDeepAttrs(element, env, ms, visited);
				}
			}
		}
		else {
			for (int i = 0; i < ei.getNumberOfFields(); i++) {
				FieldInfo fi = ei.getFieldInfo(i);

				ei.setFieldAttr(fi, ms);

				if (!Types.isBasicType(fi.getType())) {
					ElementInfo efi = env.getElementInfo(ei.get1SlotField(fi));
					setDeepAttrs(efi, env, ms, visited);
				}
			}
		}
	}

	public static Marks getDeepAttrs(ElementInfo ei, MJIEnv env, Set<ElementInfo> visited) {
		
		if (null == ei || visited.contains(ei)) return null;

		visited.add(ei);

		Marks temp = ei.getObjectAttr(Marks.class);
		StackFrame sf = env.getThreadInfo().getTopFrame();

		if (ei.isArray()) {
			for (int i = 0; i < ei.arrayLength(); i++) {
				temp = Marks.union(temp, ei.getElementAttr(i, Marks.class));
			}
			if (ei.isReferenceArray()) {
				for (int ref : ei.asReferenceArray()) {
					ElementInfo element = env.getModifiableElementInfo(ref);
					temp = Marks.union(temp, getDeepAttrs(element, env, visited));
				}
			}
		}
		else {
			for (int i = 0; i < ei.getNumberOfFields(); i++) {
				FieldInfo fi = ei.getFieldInfo(i);

				temp = Marks.union(temp, ei.getFieldAttr(fi, Marks.class));

				if (!Types.isBasicType(fi.getType())) {
					ElementInfo efi = env.getElementInfo(ei.get1SlotField(fi));
					temp = Marks.union(temp, getDeepAttrs(efi, env, visited));
				}
			}
		}

		return temp;
	}

	@Override
	public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {

		//		if (enteredMethod.isNative()) return;

		StackFrame frame = currentThread.getTopFrame();
		StackFrame caller = currentThread.getCallerStackFrame();

		ClassInfo ci = enteredMethod.getClassInfo();

		String signature;

		signature = enteredMethod.getFullName();

		if (inTestScope(signature)) {
			enterTest();
		}
	}
	
	@Override
	public void methodExited(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {

		//		if (enteredMethod.isNative()) return;

		String signature = enteredMethod.getFullName();

		if (inTestScope(signature)) {
			exitTest();
		}
	}
	@Override
	public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction ins) {
		StackFrame frame = currentThread.getTopFrame();
		StackFrame caller = currentThread.getCallerStackFrame();

		MJIEnv env = currentThread.getMJIEnv();
		AttrHelper h = new AttrHelper();

		if (env == null) return;
		if (!isEntered()) return;

		if (ins instanceof NATIVERETURN) {

			try {
				JVMNativeStackFrame nsf = (JVMNativeStackFrame) frame;
				MethodInfo mi = frame.getMethodInfo();
				String[] starg = mi.getArgumentTypeNames();
				Object[] attrs = env.getArgAttributes();
				Object[] MJIargs = nsf.getArguments();

				Marks ms = new Marks(attrs);


				for (int i = 0; i < starg.length; i++) {
					Object arg = MJIargs[i + 2];
					String type = starg[i];
					if (!Types.isBasicType(type)) {
						ElementInfo ei = env.getElementInfo((int) arg);
						ms = Marks.union(ms, getDeepAttrs(ei, env, new HashSet<ElementInfo>()));
					}
				}

//				if (frame.getMethodInfo().toString().contains("String.equals")
////									&& caller.getMethodInfo().toString().contains("append")) {
//				) {
//					System.out.println("haha"+ frame.getMethodInfo().toString() + ms + "\n" + h.unionOnStack(frame));
//				}

				if (ms.isEmpty()) ms = null;

				String retType = mi.getReturnType();

				if (Types.isReference(retType) || Types.isArray(retType)) {
					int retVal = nsf.getResult();
					ElementInfo ei = env.getElementInfo(retVal);
					setDeepAttrs(ei, env, ms, new HashSet<ElementInfo>());
				}

//				nsf.setReturnAttr(ms);
				nsf.setReturnAttr(Marks.union(ms, h.unionOnStack(frame)));

			} catch (NullPointerException e) {
				if (frame.getMethodInfo().toString().contains("format")) {
					System.out.println("failed null");
					throw e;
				}
			}
		}

	}

}
