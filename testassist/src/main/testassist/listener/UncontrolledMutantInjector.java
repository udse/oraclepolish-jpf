package testassist.listener;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Heap;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

import testassist.mark.Marks;
import testassist.mark.UncontrolledMark;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class UncontrolledMutantInjector extends UncontrolledMarkInjector {

	private Marks poisonMarks;
	Multimap<StaticElementInfo, FieldInfo> cache = ArrayListMultimap.create();

	private Random rand = new Random();


	private boolean isPoisonous(Marks ms) {
		return poisonMarks.containsAll(ms);
	}

	public UncontrolledMutantInjector(TestInfo ti, Marks poisonMarks) {
		super(ti);
		this.poisonMarks = poisonMarks;
		//		this.cache = cache;
	}

	@Override
	public void manipulateField(ThreadInfo ti, ElementInfo ei, FieldInfo fi) {

//		ThreadInfo ti = vm.getCurrentThread();
		int randint = rand.nextInt();
		double randdouble = rand.nextDouble() * randint;
		float randfloat = rand.nextFloat() * randint;
		long randlong = rand.nextLong();
		String randstring = RandomStringUtils.randomAlphabetic(Math.abs(randint % 64));

		ClassInfo ci = ei.getClassInfo();
		String className = ci.getName();

		String s = String.format(OC_FIELD, className, fi.getName());
		Marks ms = new Marks(new UncontrolledMark(s));
		if (!isPoisonous(ms)) return;
		
		ei = ei.getModifiableInstance();
		
		if (ei == null) return;
		
		System.out.println("mutate " + ms);
		if (fi.isReference()) {
			Heap heap = vm.getHeap();
			if (fi.getType().contains("String")) {
				int ref = heap.newString(randstring, ti).getObjectRef();
				ei.setReferenceField(fi, ref);
			}
			else {
//				ClassInfo oci = ClassInfo.getInitializedClassInfo("gov.nasa.jpf.vm.HappyJohn", vm.getCurrentThread());
//				int ref = heap.newObject(oci, vm.getCurrentThread()).getObjectRef();
				ei.setReferenceField(fi, -1);
			}
		}
		else if (fi.isDoubleField()) {
			ei.setDoubleField(fi, randdouble);
		}
		else if (fi.isFloatField()) {
			ei.setFloatField(fi, randfloat);
		}
		else if (fi.isIntField()) {
			ei.setIntField(fi, randint);
		}
		else if (fi.isLongField()) {
			ei.setLongField(fi, randlong);
		}
		else if (fi.isShortField()) {
			ei.setShortField(fi, (short) randint);
		}
		else if (fi.isCharField()) {
			ei.setCharField(fi, RandomStringUtils.randomAlphabetic(1).charAt(0));
		}
		else if (fi.isBooleanField()) {
			boolean v = ei.getBooleanField(fi);
			if (v) {
				ei.setBooleanField(fi, false);
			}
			else {
				ei.setBooleanField(fi, true);
			}
		}
		else if (fi.isByteField()) {
			ei.setByteField(fi, (byte) randint);
		}
	}

}
