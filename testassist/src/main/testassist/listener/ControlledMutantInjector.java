package testassist.listener;

import java.util.Collection;
import java.util.Random;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;

import com.google.common.collect.Sets;
import com.objectdb.o.ELI;

import testassist.injector.Injector;
import testassist.mark.Mark;
import testassist.mark.Marks;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.bytecode.ICONST;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Heap;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class ControlledMutantInjector extends MarkInjector {

	private Set<Mark> leftOvers;
	private Random rand = new Random();

	public ControlledMutantInjector(TestInfo ti, Set<Mark> leftOvers) {
		super(ti);
		this.leftOvers = leftOvers;
	}

	@Override
	public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction insn) {
		StackFrame frame = currentThread.getTopFrame();
		MethodInfo currentMethod = frame.getMethodInfo();
		String signature = currentMethod.getFullName();

		final Injector in = new Injector();
		int bco = insn.getPosition();
		Heap heap = vm.getHeap();

		if (inTestMethod(signature)) {
			Collection<Integer> offsets = inTaintRange(bco, signature, testInfo.taints());
			if (offsets.size() != 0) {
				for (int offset : offsets) {
					Marks m = in.createControlledMarks(frame, "@offset : " + offset + " @line : " + insn.getLineNumber());
					if (leftOvers.containsAll(m)) {
						int randint = rand.nextInt();
						String randstring = RandomStringUtils.randomAlphabetic(Math.abs(randint % 64));
						System.out.println("mutate : " + m);
						if (frame.isOperandRef(offset)) {
							int objref = frame.peek(offset);
							ElementInfo ei = currentThread.getModifiableElementInfo(objref);
							if (ei == null) continue;
							int ref = -1;
							if (ei.isStringObject()) {
								ref = heap.newString(randstring, currentThread).getObjectRef();
							}
							else if (ei.isArray()) {
								ref = heap.newArray(ei.getArrayType(), ei.getArrayFields().arrayLength(), currentThread).getObjectRef();
							}
							else {
								ClassInfo oci = ClassInfo.getInitializedClassInfo("gov.nasa.jpf.vm.HappyJohn", currentThread);
								ref = heap.newObject(oci, vm.getCurrentThread()).getObjectRef();
							}
							frame.setOperand(offset, ref, true);
						}
						else {
							int newv = randint;
							if (insn instanceof ICONST) {
								ICONST iconst = (ICONST) insn;
								if (iconst.getValue() == 1) {
									newv = 0;
								}
								else if (iconst.getValue() == 0) {
									newv = -11;
								}
								else if (iconst.getValue() > 0) {
									newv = -1;
								}
								else {
									newv = randint;
								}
							}
							frame.setOperand(offset, newv, false);
						}
					}

				}
			}
		}
	}
}
