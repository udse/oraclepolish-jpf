package testassist.listener;

import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import testassist.bytecode.arithmetic.DADD;
import testassist.bytecode.arithmetic.DDIV;
import testassist.bytecode.arithmetic.DMUL;
import testassist.bytecode.arithmetic.DNEG;
import testassist.bytecode.arithmetic.DREM;
import testassist.bytecode.arithmetic.DSUB;
import testassist.bytecode.arithmetic.FADD;
import testassist.bytecode.arithmetic.FDIV;
import testassist.bytecode.arithmetic.FMUL;
import testassist.bytecode.arithmetic.FNEG;
import testassist.bytecode.arithmetic.FREM;
import testassist.bytecode.arithmetic.FSUB;
import testassist.bytecode.arithmetic.IADD;
import testassist.bytecode.arithmetic.IAND;
import testassist.bytecode.arithmetic.IDIV;
import testassist.bytecode.arithmetic.IMUL;
import testassist.bytecode.arithmetic.INEG;
import testassist.bytecode.arithmetic.IOR;
import testassist.bytecode.arithmetic.IREM;
import testassist.bytecode.arithmetic.ISHL;
import testassist.bytecode.arithmetic.ISHR;
import testassist.bytecode.arithmetic.ISUB;
import testassist.bytecode.arithmetic.IUSHR;
import testassist.bytecode.arithmetic.IXOR;
import testassist.bytecode.arithmetic.LADD;
import testassist.bytecode.arithmetic.LAND;
import testassist.bytecode.arithmetic.LDIV;
import testassist.bytecode.arithmetic.LMUL;
import testassist.bytecode.arithmetic.LNEG;
import testassist.bytecode.arithmetic.LOR;
import testassist.bytecode.arithmetic.LREM;
import testassist.bytecode.arithmetic.LSHL;
import testassist.bytecode.arithmetic.LSHR;
import testassist.bytecode.arithmetic.LSUB;
import testassist.bytecode.arithmetic.LUSHR;
import testassist.bytecode.arithmetic.LXOR;
import testassist.bytecode.compare.DCMPG;
import testassist.bytecode.compare.DCMPL;
import testassist.bytecode.compare.FCMPG;
import testassist.bytecode.compare.FCMPL;
import testassist.bytecode.compare.LCMP;
import testassist.bytecode.conversion.D2F;
import testassist.bytecode.conversion.D2I;
import testassist.bytecode.conversion.D2L;
import testassist.bytecode.conversion.F2D;
import testassist.bytecode.conversion.F2I;
import testassist.bytecode.conversion.F2L;
import testassist.bytecode.conversion.I2B;
import testassist.bytecode.conversion.I2C;
import testassist.bytecode.conversion.I2D;
import testassist.bytecode.conversion.I2F;
import testassist.bytecode.conversion.I2L;
import testassist.bytecode.conversion.I2S;
import testassist.bytecode.conversion.L2D;
import testassist.bytecode.conversion.L2F;
import testassist.bytecode.conversion.L2I;
import testassist.mark.Marks;
import testassist.util.AttrHelper;

import com.google.common.base.Optional;

import edu.udel.testassist.configuration.ControlFlowInfo;
import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.bytecode.DCONST;
import gov.nasa.jpf.jvm.bytecode.DRETURN;
import gov.nasa.jpf.jvm.bytecode.FCONST;
import gov.nasa.jpf.jvm.bytecode.GETFIELD;
import gov.nasa.jpf.jvm.bytecode.GETSTATIC;
import gov.nasa.jpf.jvm.bytecode.ICONST;
import gov.nasa.jpf.jvm.bytecode.IFEQ;
import gov.nasa.jpf.jvm.bytecode.IFGE;
import gov.nasa.jpf.jvm.bytecode.IFGT;
import gov.nasa.jpf.jvm.bytecode.IFLE;
import gov.nasa.jpf.jvm.bytecode.IFLT;
import gov.nasa.jpf.jvm.bytecode.IFNE;
import gov.nasa.jpf.jvm.bytecode.IF_ACMPEQ;
import gov.nasa.jpf.jvm.bytecode.IF_ACMPNE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPEQ;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPGE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPGT;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPLE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPLT;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPNE;
import gov.nasa.jpf.jvm.bytecode.INSTANCEOF;
import gov.nasa.jpf.jvm.bytecode.IfInstruction;
import gov.nasa.jpf.jvm.bytecode.LCONST;
import gov.nasa.jpf.jvm.bytecode.NATIVERETURN;
import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.jvm.bytecode.PUTSTATIC;
import gov.nasa.jpf.jvm.bytecode.ReturnInstruction;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class ControlFlowListener extends MyListener {

	EntityManager em;

	private String m = "sudoku.Box.isSolved";

	private AttrHelper h = new AttrHelper();

	public ControlFlowListener(EntityManager em, TestInfo ti) {
		super(ti);
		this.em = em;
	}


	@Override
	public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {

		//		if (enteredMethod.isNative()) return;

		StackFrame frame = currentThread.getTopFrame();
		StackFrame caller = currentThread.getCallerStackFrame();

		ClassInfo ci = enteredMethod.getClassInfo();

		String signature;

		signature = enteredMethod.getFullName();

		if (inTestScope(signature)) {
			enterTest();
		}

		if (enteredMethod.isNative() || enteredMethod.isAbstract()
				|| ci.isAbstract() || ci.isInterface())
			return;
		ControlFlowInfo currentCFInfo = em.find(ControlFlowInfo.class, signature);

		if (currentCFInfo != null) {
			if (enteredMethod.toString().contains("xxxxxxxxxx"))
				System.out.println("map : " + currentCFInfo.getControlFlowMap());
			h.setSFInfo(frame, currentCFInfo.getControlFlowMap());
		}
		else {

			//			throw new RuntimeException("no cf map" + signature + "\n" + enteredMethod);
			h.setSFInfo(frame, new HashMap<Integer, List<Integer>>());
		}

		//				h.PUSHALL(frame, h.marksOnStack(caller));
	}

	@Override
	public void instructionExecuted(VM vm, ThreadInfo ti,
			Instruction nextInsn, Instruction insn) {

		StackFrame frame = ti.getTopFrame();
		StackFrame caller = ti.getCallerStackFrame();
		int bco = insn.getPosition();

		if (insn instanceof FADD ||
				insn instanceof FCONST ||
				insn instanceof FDIV ||
				insn instanceof FMUL ||
				insn instanceof FNEG ||
				insn instanceof FREM ||
				insn instanceof FSUB ||
				insn instanceof IADD ||
				insn instanceof IAND ||
				insn instanceof ICONST ||
				insn instanceof IDIV ||
				insn instanceof IMUL ||
				insn instanceof INEG ||
				insn instanceof IOR ||
				insn instanceof IREM ||
				insn instanceof ISHL ||
				insn instanceof ISHR ||
				insn instanceof ISUB ||
				insn instanceof IUSHR ||
				insn instanceof IXOR ||

				insn instanceof INSTANCEOF ||

				insn instanceof D2F ||
				insn instanceof D2I ||
				insn instanceof L2F ||
				insn instanceof L2I ||
				insn instanceof F2I ||
				insn instanceof I2F ||
				insn instanceof I2B ||
				insn instanceof I2C ||
				insn instanceof I2S)
		{
			Optional<Marks> orig = Optional.fromNullable(frame.getOperandAttr(Marks.class));
			frame.setOperandAttr(Marks.union(orig, Optional.fromNullable(h.unionOnStack(frame))));
		}

		if (insn instanceof DADD ||
				insn instanceof DDIV ||
				insn instanceof DCONST ||
				insn instanceof DMUL ||
				insn instanceof DNEG ||
				insn instanceof DREM ||
				insn instanceof DSUB ||
				insn instanceof LADD ||
				insn instanceof LAND ||
				insn instanceof LCONST ||
				insn instanceof LDIV ||
				insn instanceof LMUL ||
				insn instanceof LNEG ||
				insn instanceof LOR ||
				insn instanceof LREM ||
				insn instanceof LSHL ||
				insn instanceof LSHR ||
				insn instanceof LSUB ||
				insn instanceof LUSHR ||
				insn instanceof LXOR ||

				insn instanceof DCMPG ||
				insn instanceof DCMPL ||
				insn instanceof FCMPG ||
				insn instanceof FCMPL ||
				insn instanceof LCMP ||

				insn instanceof D2L ||
				insn instanceof F2D ||
				insn instanceof F2L ||
				insn instanceof I2D ||
				insn instanceof I2L ||
				insn instanceof L2D

		)
		{
//			if (insn instanceof DMUL && frame.getMethodInfo().toString().contains("earnings")) {
//				System.out.println(frame.getMethodInfo().toString() + orig + insn);
//			}
			Optional<Marks> orig = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
			frame.setLongOperandAttr(Marks.union(orig, Optional.fromNullable(h.unionOnStack(frame))));

		}


		h.POP(frame, -1 * bco);
	}

	@Override
	public void executeInstruction(VM vm, ThreadInfo currentThread,
			Instruction insn) {

		StackFrame frame = currentThread.getTopFrame();
		StackFrame caller = currentThread.getCallerStackFrame();

		if (insn instanceof ReturnInstruction) {
			if (insn instanceof NATIVERETURN) {
			}
			else {
				ReturnInstruction ret = (ReturnInstruction) insn;
				int size = ret.getReturnTypeSize();
				if (size > 0) {
					Optional<Marks> orig = Optional.fromNullable(frame.getOperandAttr(size - 1, Marks.class));
					frame.setOperandAttr(size - 1, Marks.union(orig, Optional.fromNullable(h.unionOnStack(frame))));
				}
			}
		}

		int bco = insn.getPosition();
		h.POP(frame, bco);

		if (insn instanceof DRETURN) {

			Optional<Marks> orig = Optional.fromNullable(frame.getOperandAttr(1, Marks.class));
		}

		if (insn instanceof GETFIELD) {
			//					if (frame.getMethodInfo().toString().contains("EqualsBuilder.append")
			//							&& caller.getMethodInfo().toString().contains("splitByCh")) {
			//							){
			GETFIELD put = (GETFIELD) insn;
			ElementInfo ei = put.peekElementInfo(currentThread);
			FieldInfo fi = put.getFieldInfo();
			if (fi.getFullName().contains("xxxxx"))
				System.out.println("GET " + ei.getClassInfo().toString() + ei.getObjectRef() + " :: " + fi.getFullName() + "\n\ton field" + ei.getFieldAttr(fi, Marks.class) + "\t\n"
						+ frame.getMethodInfo() + "@" + bco + "\tline : " + insn.getLineNumber() + "\n\tonstack" + h.unionOnStack(frame) + "\n");
			//					}
		} //		

		if (insn instanceof GETSTATIC) {
			//					if (frame.getMethodInfo().toString().contains("EqualsBuilder.append")
			//							&& caller.getMethodInfo().toString().contains("splitByCh")) {
			//							){
			GETSTATIC put = (GETSTATIC) insn;
			ElementInfo ei = put.peekElementInfo(currentThread);
			FieldInfo fi = put.getFieldInfo();
			if (fi.getFullName().contains("xxxxxx"))
				System.out.println("GET " + ei.getClassInfo().toString() + ei.getObjectRef() + " :: " + fi.getFullName() + "\n\ton field" + ei.getFieldAttr(fi, Marks.class) + "\t\n"
						+ frame.getMethodInfo() + "@" + bco + "\tline : " + insn.getLineNumber() + "\n\tonstack" + h.unionOnStack(frame) + "\n");
			//					}
		} //		

		if (insn instanceof PUTFIELD) {
			//					if (frame.getMethodInfo().toString().contains("EqualsBuilder.append")
			//							&& caller.getMethodInfo().toString().contains("splitByCh")) {
			//							){
			PUTFIELD put = (PUTFIELD) insn;
			ElementInfo ei = put.peekElementInfo(currentThread);
			FieldInfo fi = put.getFieldInfo();
			if (fi.getFullName().contains("xxx"))
				System.out.println("set " + ei.getClassInfo().toString() + " :: " + fi.getFullName() + "\n\ton field" + ei.getFieldAttr(fi, Marks.class) + "\n\ton operand "
						+ frame.getOperandAttr(Marks.class) + "\n\t"
						+ frame.getMethodInfo() + "@" + bco + "\tline : " + insn.getLineNumber() + "\n\tonstack" + h.unionOnStack(frame) + "\n");
			//					}
		}
		if (insn instanceof PUTSTATIC) {
			//					if (frame.getMethodInfo().toString().contains("EqualsBuilder.append")
			//							&& caller.getMethodInfo().toString().contains("splitByCh")) {
			//							){
			PUTSTATIC put = (PUTSTATIC) insn;
			ElementInfo ei = put.peekElementInfo(currentThread);
			FieldInfo fi = put.getFieldInfo();
			if (fi.getFullName().contains("xxxxxx"))
				System.out.println("set " + ei.getClassInfo().toString() + " :: " + fi.getFullName() + "\n\ton field" + ei.getFieldAttr(fi, Marks.class) + "\n\ton operand "
						+ frame.getOperandAttr(Marks.class) + "\n\t"
						+ frame.getMethodInfo() + "@" + bco + "\tline : " + insn.getLineNumber() + "\n\tonstack" + h.unionOnStack(frame) + "\n");
			//					}
		}
		if (!isEntered()) return;

		if (insn instanceof IfInstruction) {
			Marks ms = null;
			if (insn instanceof IFEQ ||
					insn instanceof IFGE ||
					insn instanceof IFGT ||
					insn instanceof IFLE ||
					insn instanceof IFLT ||
					insn instanceof IFNE) {

				ms = frame.getOperandAttr(Marks.class);

			}

			if (insn instanceof IF_ACMPEQ ||
					insn instanceof IF_ACMPNE ||
					insn instanceof IF_ICMPEQ ||
					insn instanceof IF_ICMPGE ||
					insn instanceof IF_ICMPGT ||
					insn instanceof IF_ICMPLE ||
					insn instanceof IF_ICMPLT ||
					insn instanceof IF_ICMPNE) {
				Optional<Marks> m1 = Optional.fromNullable(frame.getOperandAttr(Marks.class));
				Optional<Marks> m2 = Optional.fromNullable(frame.getOperandAttr(1, Marks.class));

				ms = Marks.union(m1, m2);
			}

			if (ms != null) {
				h.PUSH(frame, ms, bco);
			}

		}

	}
}
