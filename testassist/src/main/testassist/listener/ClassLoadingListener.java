package testassist.listener;

import java.util.HashSet;
import java.util.Set;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class ClassLoadingListener extends MyListener {
	
	Set<String> lateLoad = new HashSet<String>();

	public ClassLoadingListener(TestInfo ti) {
		super(ti);
	}

	@Override
	public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo mi) {
		String methodName = mi.getFullName();

		if (inTestScope(methodName)) {
			enterTest();
		}
	}


	@Override
	public void classLoaded(VM vm, ClassInfo loadedClass) {
		
		if (isEntered()) {
			lateLoad.add(loadedClass.getName());
		}

	}
	
	public Set<String> getLateLoaded() {
		return lateLoad;
	}
}
