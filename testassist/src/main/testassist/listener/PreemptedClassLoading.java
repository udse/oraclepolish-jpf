package testassist.listener;

import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

import java.util.Set;

public class PreemptedClassLoading extends ListenerAdapter{

	private Set<String> toLoad;

	public PreemptedClassLoading(Set<String> toLoad) {
		this.toLoad = toLoad;
	}
	
	private void loadClass(VM vm, String cname) {
		
		ClassLoaderInfo cl = ClassLoaderInfo.getCurrentSystemClassLoader();
		ClassInfo ci = cl.getResolvedClassInfo(cname);

		if (!ci.isRegistered()) {
			ThreadInfo ti = vm.getCurrentThread();
			ci.registerClass(ti);

			if (!ci.isInitialized()) {
				ci.initializeClass(ti);
			}
		}
//		System.out.printf("loaded: %s\n", ci.getName());
	}

	@Override
	public void vmInitialized(VM vm) {
		for (String cname : toLoad) {
			loadClass(vm, cname);
		}
	}

}
