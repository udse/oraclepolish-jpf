package testassist.listener;

import java.util.ArrayList;
import java.util.Collection;

import testassist.mark.Marks;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.bytecode.DCONST;
import gov.nasa.jpf.jvm.bytecode.FCONST;
import gov.nasa.jpf.jvm.bytecode.ICONST;
import gov.nasa.jpf.jvm.bytecode.InvokeInstruction;
import gov.nasa.jpf.jvm.bytecode.LCONST;
import gov.nasa.jpf.jvm.bytecode.LDC;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;
import gov.nasa.jpf.vm.VM;

public class AutoControlledMarkInjector extends MarkInjector {

	public AutoControlledMarkInjector(TestInfo ti) {
		super(ti);
	}

	public Marks getAllTestMarks() {
		return allTestMarks;
	}

	protected Collection<Integer> taintConstants(Instruction insn) {
		Collection<Integer> offsets = new ArrayList<Integer>();
		if (insn instanceof ICONST ||
				insn instanceof FCONST ||
				insn instanceof LDC) {
			offsets.add(0);
		}
		else if (insn instanceof DCONST ||
				insn instanceof LCONST) {
			offsets.add(1);
		}
		else if (insn instanceof InvokeInstruction) {
			InvokeInstruction invoke = (InvokeInstruction) insn;
			if (invoke.getArgSize() == 1) {
				Byte tn = Types.getBuiltinType(invoke.getReturnTypeName());
				offsets.add(Types.getTypeSize(tn));
			}
		}

		return offsets;
	}

//	private Collection<Integer> taintMethodReturn(Instruction insn) {
//		Collection<Integer> offsets = new ArrayList<Integer>();
//
//		return offsets;
//	}

	@Override
	public void instructionExecuted(VM vm, ThreadInfo ti,
			Instruction nextInsn, Instruction insn) {

		StackFrame frame = ti.getTopFrame();
		MethodInfo currentMethod = frame.getMethodInfo();
		String signature = currentMethod.getFullName();

		if (inTestMethod(signature)) {
			//			int bco = insn.getPosition();

			Collection<Integer> offsets = taintConstants(insn);
			if (!offsets.isEmpty()) {
				performTainting(frame, offsets, insn, ti, false);
			}
		}
	}

	//	@Override
	//	public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction insn) {
	//		StackFrame frame = currentThread.getTopFrame();
	//		MethodInfo currentMethod = frame.getMethodInfo();
	//		String signature = currentMethod.getFullName();
	//
	//		if (inTestMethod(signature)) {
	////			int bco = insn.getPosition();
	//
	//			Collection<Integer> offsets = taintConstants(insn);
	//			if (!offsets.isEmpty()) {
	//				performTainting(frame, offsets, insn, currentThread, false);
	//			}
	//		}
	//
	//	}
}
