package testassist.listener;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.ClassFile;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;
import gov.nasa.jpf.vm.VM;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import testassist.mark.Marks;
import testassist.mark.UncontrolledMark;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class UncontrolledMarkInjector extends MarkInjector {

	private final Multimap<ElementInfo, FieldInfo> cache;

	protected VM vm;

	public UncontrolledMarkInjector(TestInfo ti) {
		super(ti);
		cache = ArrayListMultimap.create();
	}

	@Override
	public void methodExited(VM vm, ThreadInfo currentThread, MethodInfo enteredMethod) {

		String signature = enteredMethod.getFullName();

		if (inTestScope(signature)) {
			exitTest();
		}
	}

	@Override
	public void methodEntered(VM vm, ThreadInfo currentThread, MethodInfo mi) {
		this.vm = vm;

		String methodName = mi.getFullName();

		if (inTestScope(methodName)) {
			enterTest();

			for (Map.Entry<ElementInfo, FieldInfo> e : cache.entries()) {
				ElementInfo ei = e.getKey();
				FieldInfo fi = e.getValue();
				manipulateField(currentThread, ei, fi);
			}
		}
	}

	@Override
	public void classLoaded(VM vm, ClassInfo loadedClass) {
		
		if (excludedPackage(loadedClass.getName())) return;

		ElementInfo sei = loadedClass.getStaticElementInfo();
		//		System.out.println(loadedClass);
		for (int i = 0; i < sei.getNumberOfFields(); i++) {
			FieldInfo fi = sei.getFieldInfo(i);

			if (fi.isFinal()) continue;

			cache.put(sei, fi);
		}
	}

	@Override
	public void objectCreated(VM vm, ThreadInfo currentThread, ElementInfo newObject) {

		ClassInfo ci = newObject.getClassInfo();

		if (isEntered() ||
				!ci.getName().equals(testInfo.className())) {
			return;
		}

		for (int i = 0; i < newObject.getNumberOfFields(); i++) {
			FieldInfo fi = newObject.getFieldInfo(i);

			if (fi.isFinal()) continue;

			//			cache.put(newObject, fi);
			manipulateField(currentThread, newObject, fi);
		}
	}

	private void setDeepAttribute(ThreadInfo ti, ElementInfo ei, Marks ms, Set<ElementInfo> visited) {
		if (null == ei || visited.contains(ei)) return;

		ei = ei.getModifiableInstance();

		visited.add(ei);

		ei.setObjectAttr(ms);

		if (ei.isArray()) {
			for (int i = 0; i < ei.arrayLength(); i++) {
				ei.setElementAttr(i, ms);
			}

			if (ei.isReferenceArray()) {
				for (int ref : ei.asReferenceArray()) {
					ElementInfo element = ti.getModifiableElementInfo(ref);
					setDeepAttribute(ti, element, ms, visited);
				}
			}
		}
		else {
			for (int i = 0; i < ei.getNumberOfFields(); i++) {
				FieldInfo fi = ei.getFieldInfo(i);

				ei.setFieldAttr(fi, ms);

				if (!Types.isBasicType(fi.getType())) {
					ElementInfo efi = ti.getElementInfo(ei.get1SlotField(fi));
					setDeepAttribute(ti, efi, ms, visited);
				}
			}
		}
	}

	public void setFieldAttribute(ThreadInfo ti, ElementInfo ei, FieldInfo fi, Marks ms) {
		ei.setFieldAttr(fi, ms);
		if (fi.getFullName().contains("StaticEmployee")) {
			System.out.println(ei.getFieldAttr(fi, Marks.class));
		}
		//		System.out.println(ei.getClassInfo().toString() + ei.getObjectRef() + "\t" + fi + ei.getFieldAttr(fi, Marks.class));
		if (fi.isReference()) {
			int objref = ei.getReferenceField(fi);
			ElementInfo fei = ti.getModifiableElementInfo(objref);

			setDeepAttribute(ti, fei, ms, new HashSet<ElementInfo>());
		}
	}
	
	public int sizeOfMarks() {
		return cache.entries().size();
	}

	public void manipulateField(ThreadInfo ti, ElementInfo ei, FieldInfo fi) {
//		if (fi.getFullName().contains("Option")) System.out.println(fi);
		ClassInfo ci = ei.getClassInfo();
		String className = ci.getName();

		String s = String.format(OC_FIELD, className, fi.getName());
		Marks ms = new Marks(new UncontrolledMark(s));
		if (ei.isFrozen()) ei = ei.getModifiableInstance();
		if (ei == null) return;

		setFieldAttribute(ti, ei, fi, ms);
	}
}
