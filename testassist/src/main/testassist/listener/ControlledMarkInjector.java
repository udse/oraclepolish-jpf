package testassist.listener;

import java.util.Collection;

import testassist.mark.Marks;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class ControlledMarkInjector extends MarkInjector {

	public ControlledMarkInjector(TestInfo ti) {
		super(ti);
	}
	
	public Marks getAllTestMarks() {
		return allTestMarks;
	}

	@Override
	public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction insn) {
		StackFrame frame = currentThread.getTopFrame();
		MethodInfo currentMethod = frame.getMethodInfo();
		String signature = currentMethod.getFullName();
		
		if (inTestMethod(signature)) {
			int bco = insn.getPosition();
			
			Collection<Integer> offsets = inTaintRange(bco, signature, testInfo.taints());
			if (!offsets.isEmpty()) {
				performTainting(frame, offsets, insn, currentThread, false);
			}
		}
	}
}
