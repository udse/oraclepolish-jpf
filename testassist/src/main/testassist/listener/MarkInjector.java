package testassist.listener;

import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.MethodInfo;

public abstract class MarkInjector extends MyListener {
	
	protected String OC_ARR = "%s:%s[%d] @ %s:%s line : %d";
//	protected String OC_FIELD = "INSTANCE - %s[%s].%s @ %s:%d line : %d";	
	protected String OC_FIELD = "%s.%s";
	protected String CONST_CSTOR = "(outside the cstor) ins: %s objref :%d from %s @ %d line : %d";

	public MarkInjector(TestInfo ti) {
		super(ti);
	}

	protected boolean notApplication(MethodInfo mi) {
		return excludedPackage(mi.getFullName());
	}

	protected boolean notApplication(FieldInfo fi) {
		return excludedPackage(fi.getFullName());
	}

	protected boolean notApplication(ElementInfo ei) {

		return excludedPackage(ei.getClassInfo().getName());
	}

	protected boolean isApplication(MethodInfo mi) {
		return !excludedPackage(mi.getFullName());
	}

	protected boolean isApplication(ElementInfo ei) {
		return !excludedPackage(ei.getClassInfo().getName());
	}
}
