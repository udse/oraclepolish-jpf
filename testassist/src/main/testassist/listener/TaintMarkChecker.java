package testassist.listener;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import com.google.common.base.Joiner;
import com.google.common.collect.Sets;

import testassist.mark.Marks;
import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.jvm.bytecode.ArrayLoadInstruction;
import gov.nasa.jpf.jvm.bytecode.ArrayStoreInstruction;
import gov.nasa.jpf.jvm.bytecode.IFEQ;
import gov.nasa.jpf.jvm.bytecode.IFGE;
import gov.nasa.jpf.jvm.bytecode.IFGT;
import gov.nasa.jpf.jvm.bytecode.IFLE;
import gov.nasa.jpf.jvm.bytecode.IFLT;
import gov.nasa.jpf.jvm.bytecode.IFNE;
import gov.nasa.jpf.jvm.bytecode.IF_ACMPEQ;
import gov.nasa.jpf.jvm.bytecode.IF_ACMPNE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPEQ;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPGE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPGT;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPLE;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPLT;
import gov.nasa.jpf.jvm.bytecode.IF_ICMPNE;
import gov.nasa.jpf.jvm.bytecode.IfInstruction;
import gov.nasa.jpf.jvm.bytecode.LongArrayLoadInstruction;
import gov.nasa.jpf.jvm.bytecode.LongArrayStoreInstruction;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class TaintMarkChecker extends MyListener {

	private Marks allMarks = new Marks();
	private Marks allStructural = new Marks();

	//	private Marks structuralSMarks = new Marks();

	public Marks getComputationMarks() {
		return allMarks;
	}

	public Marks getStructuralMarks() {
		return allStructural;
	}

	public TaintMarkChecker(TestInfo ti) {
		super(ti);
	}

	@Override
	public void executeInstruction(VM vm, ThreadInfo currentThread, Instruction insn) {

		StackFrame frame = currentThread.getTopFrame();
		MethodInfo currentMethod = frame.getMethodInfo();
		String signature = currentMethod.getFullName();

		int bco = currentThread.getPC().getPosition();

		if (inTestMethod(signature)) {
			if (inRanges(bco)) {
				enterOracle();
			}
			else {
				exitOracle();
			}
		}

		if (insn instanceof ArrayLoadInstruction) {
			int size = 1;
			if (insn instanceof LongArrayLoadInstruction) {
				size = 2;
			}

			Marks ms = frame.getOperandAttr(size, Marks.class);

			if (ms == null) return;

			allStructural.addAll(ms.getControlledMarks());
		}

		if (insn instanceof ArrayStoreInstruction) {
			int size = 1;
			if (insn instanceof LongArrayStoreInstruction) {
				size = 2;
			}

			Marks ms = frame.getOperandAttr(size - 1, Marks.class);

			if (ms == null) return;

			allStructural.addAll(ms.getControlledMarks());
		}

		if (inOracle()) {
			if (insn instanceof IfInstruction) {
				Marks ms = null;
				if (insn instanceof IFEQ ||
						insn instanceof IFGE ||
						insn instanceof IFGT ||
						insn instanceof IFLE ||
						insn instanceof IFLT ||
						insn instanceof IFNE) {

					ms = frame.getOperandAttr(Marks.class);

				}

				if (insn instanceof IF_ACMPEQ ||
						insn instanceof IF_ACMPNE ||
						insn instanceof IF_ICMPEQ ||
						insn instanceof IF_ICMPGE ||
						insn instanceof IF_ICMPGT ||
						insn instanceof IF_ICMPLE ||
						insn instanceof IF_ICMPLT ||
						insn instanceof IF_ICMPNE) {
					Marks m1 = frame.getOperandAttr(Marks.class);
					Marks m2 = frame.getOperandAttr(1, Marks.class);

					ms = Marks.union(m1, m2);
				}

				if (ms != null) {
					allMarks.addAll(ms);
					//					System.out.println("found @ " + frame.getMethodInfo().toString() + " bco : " + bco + " line " + insn.getLineNumber());
				}
			}

		}
	}

	public void writeToFile(PrintWriter w) {
		w.printf(String.format("%s\n%s\n", testInfo.className()+"."+testInfo.methodName(), 
				Joiner.on("\t\n").join(allMarks.getUncontrolledMarks())));
	}

	public String toCSV() {
		return String.format("%s, %s",  
				Sets.union(allMarks.getControlledMarks(), allStructural.getControlledMarks()).size(),
				allMarks.getSizeOfUncontrolledMarks());
	}
}
