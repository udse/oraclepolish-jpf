package testassist.listener;

import edu.udel.testassist.configuration.Range;
import edu.udel.testassist.configuration.TaintInfo;
import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.ListenerAdapter;
import gov.nasa.jpf.jvm.bytecode.IINC;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import testassist.injector.Injector;
import testassist.mark.Marks;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

public class MyListener extends ListenerAdapter {

	private void setDeepAttrs(ElementInfo ei, ThreadInfo ti, Marks ms, Set<ElementInfo> visited) {
		//				if (ms != null) ms = ms.filterAMarks();
		if (null == ei || visited.contains(ei)) return;

		ei = ei.getModifiableInstance();

		visited.add(ei);

		ei.setObjectAttr(ms);

		if (ei.isArray()) {
			for (int i = 0; i < ei.arrayLength(); i++) {
				ei.setElementAttr(i, ms);
				//				System.out.println("a tainting : " + ei + ms);
			}

			if (ei.isReferenceArray()) {
				for (int ref : ei.asReferenceArray()) {
					ElementInfo element = ti.getModifiableElementInfo(ref);
					setDeepAttrs(element, ti, ms, visited);
				}
			}
		}
		else {
			for (int i = 0; i < ei.getNumberOfFields(); i++) {
				FieldInfo fi = ei.getFieldInfo(i);

				ei.setFieldAttr(fi, ms);
				//				System.out.println("f tainting : " + ei + fi + ms);

				if (!Types.isBasicType(fi.getType())) {
					ElementInfo efi = ti.getElementInfo(ei.get1SlotField(fi));
					setDeepAttrs(efi, ti, ms, visited);
				}
			}
		}
	}

	protected TestInfo testInfo = null;

	//	static protected Marks staticFinalFieldMark =
	//			new Marks(new TMark("static final field"));

	private final List<String> EXCLUDE_PREFIXES = ImmutableList.of(
			"$",
			"java.",
			"javax.",
			"com.sun.",
			"sun.",
			"org.omg.",
			"junit.",
			"org.junit.",
			"com.apple.",
			"com.intellij.",
			"org.netbeans.",
			"edu.udel.",
			"testassist.",
			"com.google."
			);

	protected boolean excludedPackage(final String name) {
		return Iterables.any(EXCLUDE_PREFIXES, new Predicate<String>() {
			public boolean apply(String s) {
				return name.startsWith(s);
			}
		});
	}

	protected Marks allTestMarks = new Marks();

	protected boolean inOracle = false;

	protected void performTainting(final StackFrame sf, final Collection<Integer> offsets,
			final Instruction ins, final ThreadInfo ti, final boolean isSetup) {
		final Injector in = new Injector();

		for (Integer offset : offsets) {
			if (ins.getLineNumber() == 0) continue;

			Marks m = in.createControlledMarks(sf, "@offset : " + offset + " @line : " + ins.getLineNumber());

			if (!isSetup) allTestMarks.addAll(m);
			if (isSetup) m = in.createControlledMarks(sf, "@offset : " + offset + " @line : " + ins.getLineNumber());

			if (ins instanceof IINC) {
				IINC inc = (IINC) ins;
				int index = inc.getIndex();
				sf.setLocalAttr(index, m);
			}
			else {
				if (sf.isOperandRef(offset)) {
					int objref = sf.peek(offset);
					ElementInfo ei = ti.getModifiableElementInfo(objref);
					if (ei != null) {
						if (ei.isStringObject()) {
						}
						setDeepAttrs(ei, ti, m, new HashSet<ElementInfo>());
					}
				}
				sf.setOperandAttr(offset, m);
			}
		}
	}

	protected boolean inOracle() {
		return inOracle;
	}

	protected void enterOracle() {
		inOracle = true;
	}

	protected void exitOracle() {
		inOracle = false;
	}

	public MyListener(TestInfo ti) {
		this.testInfo = ti;
	}

	private boolean entered = false;

	public static boolean gentered = false;

	protected void enterTest() {
		entered = true;
		gentered = true;
	}

	protected void exitTest() {
		entered = false;
		gentered = false;
	}

	protected boolean isEntered() {
		return entered;
	}

	protected boolean inTestMethod(String signature) {
		return signature.equals(testInfo.identifier().toString());
	}
	
	public static boolean ginTestMethod = false;

	protected boolean inTestScope(String signature) {
		boolean in =  signature.equals(testInfo.identifier().toString())
				|| signature.contains("setUp()")
				|| signature.contains("setup()");
		
		ginTestMethod = in;
		return in;
	}

	protected Collection<Integer> inTaintRange(int bco, String signature, Collection<TaintInfo> taints) {
		Collection<Integer> offsets = new ArrayList<Integer>();
		for (TaintInfo tp : taints) {
			if (tp.bco() == bco) {
				offsets.addAll(tp.offsets());
			}
		}
		return offsets;
	}

	protected boolean inRanges(int lineNumber) {
		for (Range r : testInfo.ranges()) {
			if (r.inRange(lineNumber)) {
				return true;
			}
		}
		return false;
	}

}
