package testassist.annotation;

public class TestAssist {

	public static boolean taint(boolean b) {
		return b;
	}

	public static byte taint(byte b) {
		return b;
	}

	public static char taint(char c) {
		return c;
	}

	public static double taint(double d) {
		return d;
	}

	public static float taint(float f) {
		return f;
	}

	public static long taint(long l) {
		return l;
	}

	public static int taint(int i) {
		return i;
	}

	public static String taint(String s) {
		return s;
	}

	
	
	public static void startTest() {
		return;
	}

	public static void startChecks() {
		return;
	}

	public static void endTest() {
		return;
	}

}
