package testassist;

import org.junit.runner.JUnitCore;
import org.junit.runner.Request;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import edu.udel.testassist.configuration.MethodIdentifier;

import testassist.util.TestFailureException;
import testassist.util.TestSuccessException;


public class SingleTestRunner {

	public static void main(String[] args) throws ClassNotFoundException {
		MethodIdentifier methodIdentifier = new MethodIdentifier(args[0]);

		Request request = Request.method(Class.forName(methodIdentifier.className()), methodIdentifier.methodName());

		Result result = new JUnitCore().run(request);

		System.out.printf(result.wasSuccessful() ? "Success" : "Failure");
		if (!result.wasSuccessful()) {
			for (Failure f : result.getFailures()) {
				System.out.println(f.getDescription());
				System.out.println(f.getException());
			}
		}
		
		if(!result.wasSuccessful()) 
			throw new TestFailureException();
		else
			throw new TestSuccessException();
		
	}
}