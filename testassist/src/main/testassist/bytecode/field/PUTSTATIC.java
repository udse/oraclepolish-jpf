package testassist.bytecode.field;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.listener.MyListener;
import testassist.mark.Marks;

public class PUTSTATIC extends gov.nasa.jpf.jvm.bytecode.PUTSTATIC {

	public PUTSTATIC(String fieldName, String clsName, String fieldDescriptor) {
		super(fieldName, clsName, fieldDescriptor);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
//		if (!MyListener.ginTestMethod) {
//			StackFrame sf = ti.getTopFrame();
//			StaticElementInfo sei = this.getLastElementInfo();
//			FieldInfo fi = this.getFieldInfo();
//			Optional<Marks> ms = Optional.fromNullable(sei.getFieldAttr(fi, Marks.class));
//
//			Optional<Marks> orig = Optional.fromNullable(sf.getOperandAttr(size-1, Marks.class));
//			
//			sf.setOperandAttr(size-1, Marks.union(ms, orig));
//		}

		return super.execute(ti);
	}

}
