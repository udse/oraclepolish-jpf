package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;

import com.google.common.base.Optional;

public class I2C extends gov.nasa.jpf.jvm.bytecode.I2C {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> attr = Optional.fromNullable(frame.getOperandAttr(Marks.class));

		int v = frame.pop();

		frame.push((char) v);

		if (attr.isPresent()) {
			frame.setOperandAttr(attr.get());
		}

		return getNext(ti);
	}

}
