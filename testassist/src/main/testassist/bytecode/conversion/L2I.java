package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class L2I extends gov.nasa.jpf.jvm.bytecode.L2I {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long value = frame.popLong();



		frame.push((int) value);
		frame.setOperandAttr(marks.orNull());

		return getNext(ti);
	}

}
