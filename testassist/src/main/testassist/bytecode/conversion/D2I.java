package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;

import com.google.common.base.Optional;

public class D2I extends gov.nasa.jpf.jvm.bytecode.D2I {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> ms = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));

		double v = frame.popDouble();
		frame.push((int) v);

		if (ms.isPresent()) {
			frame.setOperandAttr(ms.get());
		}

		return getNext(ti);
	}

}
