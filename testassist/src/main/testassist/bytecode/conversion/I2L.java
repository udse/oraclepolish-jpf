package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class I2L extends gov.nasa.jpf.jvm.bytecode.I2L {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		int value = frame.pop();
		
		frame.pushLong((long) value);
		frame.setLongOperandAttr(marks.orNull());
	
		return getNext(ti);
	}

}
