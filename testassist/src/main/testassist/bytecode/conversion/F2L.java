package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;

import com.google.common.base.Optional;

public class F2L extends gov.nasa.jpf.jvm.bytecode.F2L {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> attr = Optional.fromNullable(frame.getOperandAttr(Marks.class));

		float v = frame.popFloat();
		frame.pushLong((long) v);

		if (attr.isPresent()) {
			frame.setLongOperandAttr(attr.get());
		}

		return getNext(ti);
	}

}
