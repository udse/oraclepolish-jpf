package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.Set;

import testassist.mark.Mark;

import com.google.common.base.Optional;

public class I2S extends gov.nasa.jpf.jvm.bytecode.I2S {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		@SuppressWarnings("unchecked")
		Optional<Set<Mark>> attr = Optional.fromNullable((Set<Mark>) frame.getOperandAttr());

		int v = frame.pop();

		frame.push((short) v);

		if (attr.isPresent()) {
			frame.setOperandAttr(attr.get());
		}

		return getNext(ti);
	}

}
