package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import java.util.Set;

import testassist.mark.Mark;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class L2D extends gov.nasa.jpf.jvm.bytecode.L2D {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long v = frame.popLong();
		frame.pushDouble((double) v);

		if (marks.isPresent()) {
			frame.setLongOperandAttr(marks.get());
		}

		return getNext(ti);
	}

}
