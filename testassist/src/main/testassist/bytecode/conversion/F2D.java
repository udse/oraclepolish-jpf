package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;

import com.google.common.base.Optional;

public class F2D extends gov.nasa.jpf.jvm.bytecode.F2D {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> attr = Optional.fromNullable(frame.getOperandAttr(Marks.class));

		float f = frame.popFloat();
		frame.pushDouble((double) f);

		if (attr.isPresent()) {
			frame.setLongOperandAttr(attr.get());
		}

		return getNext(ti);
	}

}
