package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;

import com.google.common.base.Optional;

public class I2D extends gov.nasa.jpf.jvm.bytecode.I2D {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		Optional<Marks> attr = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		
		int v = frame.pop();

		frame.pushDouble((double) v);
		
		if(attr.isPresent()) {
			frame.setLongOperandAttr(attr.get());
		}
		
		return getNext(ti);
	}

}
