package testassist.bytecode.conversion;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Mark;
import testassist.mark.Marks;
import testassist.util.AttrHelper;

import com.google.common.base.Optional;

public class I2F extends gov.nasa.jpf.jvm.bytecode.I2F {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();
		


		Optional<Marks> attr = Optional.fromNullable(frame.getOperandAttr(Marks.class));

		int v = frame.pop();

		frame.pushFloat((float) v);

		
		if (attr.isPresent()) {
			frame.setOperandAttr(attr.get());
		}


		return getNext(ti);
	}

}
