package testassist.bytecode.ret;

import testassist.mark.Marks;
import testassist.util.Global;
import gov.nasa.jpf.jvm.bytecode.InstructionVisitor;
import gov.nasa.jpf.jvm.bytecode.ReturnInstruction;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class RETURN extends ReturnInstruction {
	@Override
	public Instruction execute(ThreadInfo ti) {

		// Constructors don't return anything so this is the only instruction that can be used to return from a constructor.

		//MethodInfo mi = ti.getMethod();  // Get the current method being executed (e.g. returned from).
		StackFrame sf = ti.getTopFrame();

		if (mi.isInit()) { // Check to see if this method is a constructor.

			int objref = ti.getThis();
			ElementInfo ei = ti.getElementInfo(objref); // Get the object.
			if (ei != null && !ei.isFrozen()) {
				ei.setObjectAttr(sf.getFrameAttr(Marks.class));
			}

			if (ei != null && !ei.isConstructed()) { // Don't bother doing the following work if the object is already constructed.

				ClassInfo ei_ci = ei.getClassInfo(); // Get the object's class.
				ClassInfo mi_ci = mi.getClassInfo(); // Get the method's class.

				if (ei_ci == mi_ci) { // If the object's class and the method's class are equal, then the thread is returning from the object's constructor.
					ei = ei.getModifiableInstance();
					ei.setConstructed();
				}
			}
		}
		else if (!mi.isStatic()) {
			int objref = ti.getThis();
			ElementInfo ei = ti.getElementInfo(objref); // Get the object.
			if (ei != null)
				if (!ei.isFrozen())
					ei.setObjectAttr(sf.getFrameAttr(Marks.class));
				else {
					ElementInfo eirw = ti.getHeap().getModifiable(objref);
					eirw.setObjectAttr(sf.getFrameAttr(Marks.class));
				}
		}

		return super.execute(ti);
	}

	public int getReturnTypeSize() {
		return 0;
	}

	protected Object getReturnedOperandAttr(StackFrame frame) {
		return null;
	}


	protected void getAndSaveReturnValue(StackFrame frame) {
		// we don't have any
	}

	protected void pushReturnValue(StackFrame frame) {
		// nothing to do
	}

	public Object getReturnValue(ThreadInfo ti) {
		//return Void.class; // Hmm, not sure if this is right, but we have to distinguish from ARETURN <null>
		return null;
	}

	public String toString() {
		return "return  " + mi.getFullName();
	}

	public int getByteCode() {
		return 0xB1;
	}

	public void accept(InstructionVisitor insVisitor) {
		insVisitor.visit(this);
	}

	public Object getReturnAttr(ThreadInfo ti) {
		StackFrame sf = ti.getTopFrame();
		return sf.getFrameAttr(); // no return value
	}
}
