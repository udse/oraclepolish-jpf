package testassist.bytecode.ret;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ChoiceGenerator;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.VM;

public class DRETURN extends gov.nasa.jpf.jvm.bytecode.DRETURN {
	@Override
	public Instruction execute(ThreadInfo ti) {

		if (!ti.isFirstStepInsn()) {
			ti.leave(); // takes care of unlocking before potentially creating a CG

			if (mi.isSynchronized()) {
				int objref = mi.isStatic() ? mi.getClassInfo().getClassObjectRef() : ti.getThis();
				ElementInfo ei = ti.getElementInfo(objref);

				if (ei.getLockCount() == 0) {
					ei = ei.getInstanceWithUpdatedSharedness(ti);
					if (ei.isShared()) {
						VM vm = ti.getVM();
						ChoiceGenerator<ThreadInfo> cg = vm.getSchedulerFactory().createSyncMethodExitCG(ei, ti);
						if (cg != null) {
							if (vm.setNextChoiceGenerator(cg)) {
								ti.skipInstructionLogging();
								return this; // re-enter
							}
						}
					}
				}
			}
		}

		StackFrame frame = ti.getModifiableTopFrame();
		returnFrame = frame;
		Object attr = getReturnedOperandAttr(frame); // the return attr - get this before we pop
		getAndSaveReturnValue(frame);

		// note that this is never the first frame, since we start all threads (incl. main)
		// through a direct call
		frame = ti.popAndGetModifiableTopFrame();

		// remove args, push return value and continue with next insn
		// (DirectCallStackFrames don't use this)
		frame.removeArguments(mi);
		pushReturnValue(frame);

		Marks marksOnStackFrame = returnFrame.getFrameAttr(Marks.class);

		if (marksOnStackFrame != null) {
			if (attr != null && attr instanceof Marks) {
				marksOnStackFrame.addAll((Marks) attr);
				setReturnAttr(ti, marksOnStackFrame);
			} else {
				setReturnAttr(ti, marksOnStackFrame);
			}
		}
		else {
			if (attr != null) {
				setReturnAttr(ti, attr);
			}
		}

		return frame.getPC().getNext();
	}

}
