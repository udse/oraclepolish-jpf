package testassist.bytecode.ret;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class NATIVERETURN extends gov.nasa.jpf.jvm.bytecode.NATIVERETURN {

	private Object retAttr;

	@Override
	public Instruction execute(ThreadInfo ti) {
		if (!ti.isFirstStepInsn()) {
			ti.leave(); // takes care of unlocking before potentially creating a CG
			// NativeMethodInfo is never synchronized, so no thread CG here
		}

		StackFrame frame = ti.getModifiableTopFrame();
		getAndSaveReturnValue(frame);

		// NativeStackFrame can never can be the first stack frame, so no thread CG

		frame = ti.popAndGetModifiableTopFrame();

		// remove args, push return value and continue with next insn
		frame.removeArguments(mi);
		pushReturnValue(frame);

		if (retAttr != null) {
			try {
				setReturnAttr(ti, retAttr);
			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.println("array : " + ti.getTopFrameMethodInfo());
			}
		}

		return frame.getPC().getNext();
	}

}
