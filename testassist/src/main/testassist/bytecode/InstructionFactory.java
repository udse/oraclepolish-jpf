package testassist.bytecode;

import gov.nasa.jpf.jvm.bytecode.PUTFIELD;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.bytecode.arithmetic.DADD;
import testassist.bytecode.arithmetic.DDIV;
import testassist.bytecode.arithmetic.DMUL;
import testassist.bytecode.arithmetic.DNEG;
import testassist.bytecode.arithmetic.DREM;
import testassist.bytecode.arithmetic.DSUB;
import testassist.bytecode.arithmetic.FADD;
import testassist.bytecode.arithmetic.FDIV;
import testassist.bytecode.arithmetic.FMUL;
import testassist.bytecode.arithmetic.FNEG;
import testassist.bytecode.arithmetic.FREM;
import testassist.bytecode.arithmetic.FSUB;
import testassist.bytecode.arithmetic.IADD;
import testassist.bytecode.arithmetic.IAND;
import testassist.bytecode.arithmetic.IDIV;
import testassist.bytecode.arithmetic.IMUL;
import testassist.bytecode.arithmetic.INEG;
import testassist.bytecode.arithmetic.IOR;
import testassist.bytecode.arithmetic.IREM;
import testassist.bytecode.arithmetic.ISHL;
import testassist.bytecode.arithmetic.ISHR;
import testassist.bytecode.arithmetic.ISUB;
import testassist.bytecode.arithmetic.IUSHR;
import testassist.bytecode.arithmetic.IXOR;
import testassist.bytecode.arithmetic.LADD;
import testassist.bytecode.arithmetic.LAND;
import testassist.bytecode.arithmetic.LDIV;
import testassist.bytecode.arithmetic.LMUL;
import testassist.bytecode.arithmetic.LNEG;
import testassist.bytecode.arithmetic.LOR;
import testassist.bytecode.arithmetic.LREM;
import testassist.bytecode.arithmetic.LSHL;
import testassist.bytecode.arithmetic.LSHR;
import testassist.bytecode.arithmetic.LSUB;
import testassist.bytecode.arithmetic.LUSHR;
import testassist.bytecode.arithmetic.LXOR;
import testassist.bytecode.array.AASTORE;
import testassist.bytecode.compare.DCMPG;
import testassist.bytecode.compare.DCMPL;
import testassist.bytecode.compare.INSTANCEOF;
import testassist.bytecode.compare.LCMP;
import testassist.bytecode.conversion.D2F;
import testassist.bytecode.conversion.D2I;
import testassist.bytecode.conversion.D2L;
import testassist.bytecode.conversion.F2D;
import testassist.bytecode.conversion.F2I;
import testassist.bytecode.conversion.F2L;
import testassist.bytecode.conversion.I2B;
import testassist.bytecode.conversion.I2C;
import testassist.bytecode.conversion.I2D;
import testassist.bytecode.conversion.I2F;
import testassist.bytecode.conversion.I2L;
import testassist.bytecode.conversion.I2S;
import testassist.bytecode.conversion.L2D;
import testassist.bytecode.conversion.L2F;
import testassist.bytecode.conversion.L2I;
import testassist.bytecode.field.PUTSTATIC;
import testassist.bytecode.load.AALOAD;
import testassist.bytecode.ret.NATIVERETURN;
import testassist.mark.Marks;

import com.google.common.base.Optional;
//import testassist.bytecode.arithmetic.IINC;
//import testassist.bytecode.invoke.INVOKESTATIC;
//import testassist.bytecode.invoke.INVOKEVIRTUAL;

public class InstructionFactory extends gov.nasa.jpf.jvm.bytecode.InstructionFactory {
	//	@Override
	//	public Instruction aconst_null() {
	//		return new ACONST_NULL();
	//	}

//	@Override
//	public Instruction arraylength() {
//		return new ARRAYLENGTH();
//	}
	
	@Override
	public Instruction aaload() {
		return new AALOAD();
	}

	@Override
	public Instruction d2f() {
		return new D2F();
	}

	@Override
	public Instruction d2i() {
		return new D2I();
	}

	@Override
	public Instruction d2l() {
		return new D2L();
	}

	@Override
	public Instruction dadd() {
		return new DADD();
	}

	//	@Override
	//	public Instruction dconst_0() {
	//		return new DCONST(0.0);
	//	}
	//
	//	@Override
	//	public Instruction dconst_1() {
	//		return new DCONST(1.0);
	//	}

	@Override
	public Instruction dcmpg() {
		return new DCMPG();
	}

	@Override
	public Instruction dcmpl() {
		return new DCMPL();
	}

	@Override
	public Instruction ddiv() {
		return new DDIV();
	}

	@Override
	public Instruction dmul() {
		return new DMUL();
	}

	@Override
	public Instruction dneg() {
		return new DNEG();
	}

	@Override
	public Instruction drem() {
		return new DREM();
	}

	@Override
	public Instruction dsub() {
		return new DSUB();
	}

	//	@Override
	//	public Instruction dup() {
	//		return new DUP();
	//	}

	@Override
	public Instruction f2d() {
		return new F2D();
	}

	@Override
	public Instruction f2i() {
		return new F2I();
	}

	@Override
	public Instruction f2l() {
		return new F2L();
	}

	@Override
	public Instruction fadd() {
		return new FADD();
	}

	//	@Override
	//	public Instruction fconst_0() {
	//		return new FCONST(0.0f);
	//	}
	//
	//	@Override
	//	public Instruction fconst_1() {
	//		return new FCONST(1.0f);
	//	}
	//
	//	@Override
	//	public Instruction fconst_2() {
	//		return new FCONST(2.0f);
	//	}
	@Override
	public Instruction nativereturn() {
		return new NATIVERETURN();
	}

	@Override
	public Instruction fdiv() {
		return new FDIV();
	}

	@Override
	public Instruction fmul() {
		return new FMUL();
	}

	@Override
	public Instruction fneg() {
		return new FNEG();
	}

	@Override
	public Instruction frem() {
		return new FREM();
	}

	@Override
	public Instruction fsub() {
		return new FSUB();
	}

	@Override
	public Instruction l2d() {
		return new L2D();
	}

	@Override
	public Instruction l2f() {
		return new L2F();
	}

	@Override
	public Instruction l2i() {
		return new L2I();
	}

	@Override
	public Instruction ladd() {
		return new LADD();
	}

	@Override
	public Instruction land() {
		return new LAND();
	}
	//	@Override
	//	public Instruction lconst_0() {
	//		return new LCONST(0);
	//	}
	//
	//	@Override
	//	public Instruction lconst_1() {
	//		return new LCONST(1L);
	//	}

	@Override
	public Instruction ldiv() {
		return new LDIV();
	}

	@Override
	public Instruction lmul() {
		return new LMUL();
	}

	@Override
	public Instruction lneg() {
		return new LNEG();
	}

	@Override
	public Instruction lor() {
		return new LOR();
	}

	@Override
	public Instruction lrem() {
		return new LREM();
	}

	@Override
	public Instruction lshl() {
		return new LSHL();
	}

	@Override
	public Instruction lshr() {
		return new LSHR();
	}

	@Override
	public Instruction lsub() {
		return new LSUB();
	}

	@Override
	public Instruction lushr() {
		return new LUSHR();
	}

	@Override
	public Instruction lcmp() {
		return new LCMP();
	}

	@Override
	public Instruction lxor() {
		return new LXOR();
	}

	@Override
	public Instruction i2b() {
		return new I2B();
	}

	@Override
	public Instruction i2c() {
		return new I2C();
	}

	@Override
	public Instruction i2d() {
		return new I2D();
	}

	@Override
	public Instruction i2f() {
		return new I2F();
	}

	@Override
	public Instruction i2l() {
		return new I2L();
	}

	@Override
	public Instruction i2s() {
		return new I2S();
	}

	//	@Override
	//	public Instruction iconst_m1() {
	//		return new ICONST(-1);
	//	}
	//
	//	@Override
	//	public Instruction iconst_0() {
	//		return new ICONST(0);
	//	}
	//
	//	@Override
	//	public Instruction iconst_1() {
	//		return new ICONST(1);
	//	}
	//
	//	@Override
	//	public Instruction iconst_2() {
	//		return new ICONST(2);
	//	}
	//
	//	@Override
	//	public Instruction iconst_3() {
	//		return new ICONST(3);
	//	}
	//
	//	@Override
	//	public Instruction iconst_4() {
	//		return new ICONST(4);
	//	}
	//
	//	@Override
	//	public Instruction iconst_5() {
	//		return new ICONST(5);
	//	}

	//
	//	@Override
	//	public Instruction istore(int index) {
	//		return new ISTORE(index);
	//	}
	//
	//	@Override
	//	public Instruction istore_0() {
	//		return new ISTORE(0);
	//	}
	//
	//	@Override
	//	public Instruction istore_1() {
	//		return new ISTORE(1);
	//	}
	//
	//	@Override
	//	public Instruction istore_2() {
	//		return new ISTORE(2);
	//	}
	//
	//	@Override
	//	public Instruction istore_3() {
	//		return new ISTORE(3);
	//	}
	//	@Override
	//	public Instruction iaload() {
	//		return new IALOAD();
	//	}

	@Override
	public Instruction iadd() {
		return new IADD();
	}

	@Override
	public Instruction iand() {
		return new IAND();
	}

	@Override
	public Instruction idiv() {
		return new IDIV();
	}

	@Override
	public Instruction instanceof_(String clsName) {
		return new INSTANCEOF(clsName);
	}

//	@Override
//	public Instruction if_acmpeq(int targetPc) {
//		return new IF_ACMPEQ(targetPc);
//	}
//
//	@Override
//	public Instruction if_acmpne(int targetPc) {
//		return new IF_ACMPNE(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmpeq(int targetPc) {
//		return new IF_ICMPEQ(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmpne(int targetPc) {
//		return new IF_ICMPNE(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmplt(int targetPc) {
//		return new IF_ICMPLT(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmpge(int targetPc) {
//		return new IF_ICMPGE(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmpgt(int targetPc) {
//		return new IF_ICMPGT(targetPc);
//	}
//
//	@Override
//	public Instruction if_icmple(int targetPc) {
//		return new IF_ICMPLE(targetPc);
//	}

	//	@Override
	//	public Instruction ifeq(int targetPc) {
	//		return new IFEQ(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifne(int targetPc) {
	//		return new IFNE(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction iflt(int targetPc) {
	//		return new IFLT(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifle(int targetPc) {
	//		return new IFLE(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifgt(int targetPc) {
	//		return new IFGT(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifge(int targetPc) {
	//		return new IFGE(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifnonnull(int targetPc) {
	//		return new IFNONNULL(targetPc);
	//	}
	//
	//	@Override
	//	public Instruction ifnull(int targetPc) {
	//		return new IFNULL(targetPc);
	//	}

	//	@Override
	//	public Instruction iinc(int localVarIndex, int incConstant) {
	//		return new IINC(localVarIndex, incConstant);
	//	}

	@Override
	public Instruction imul() {
		return new IMUL();
	}

	@Override
	public Instruction ineg() {
		return new INEG();
	}

	@Override
	public Instruction ior() {
		return new IOR();
	}

	@Override
	public Instruction irem() {
		return new IREM();
	}

	@Override
	public Instruction ishl() {
		return new ISHL();
	}

	@Override
	public Instruction ishr() {
		return new ISHR();
	}

	@Override
	public Instruction isub() {
		return new ISUB();
	}

	@Override
	public Instruction iushr() {
		return new IUSHR();
	}

	@Override
	public Instruction ixor() {
		return new IXOR();
	}
	
  @Override
  public Instruction putstatic(String fieldName, String clsName, String fieldDescriptor){
    return new PUTSTATIC(fieldName, clsName, fieldDescriptor);
  }
	//	@Override
	//	public Instruction new_(String clsName) {
	//		return new NEW(clsName);
	//	}

//		@Override
//		public Instruction putfield(String fieldName, String clsName, String fieldDescriptor) {
//			return new PUTFIELD(fieldName, clsName, fieldDescriptor) {
//				public Instruction execute (ThreadInfo ti) {
//
//					StackFrame frame = ti.getTopFrame();
//					FieldInfo fi = getFieldInfo();
//					if (fi.getFullName().contains("Employee.firstName")) {
//						
//						if(1 == size) {
//							Optional<Marks> valueMarks = Optional.fromNullable(frame.getOperandAttr(Marks.class));
//							int objRef = frame.peek(size);
//							ElementInfo ei = ti.getElementInfo(objRef);
//							Optional<Marks> fieldMarks = Optional.fromNullable(ei.getFieldAttr(fi, Marks.class));
//
//							System.out.printf("Overwriting %s[%d].%s: %s <- %s @ %s:%d\n",
//									ei.getClassInfo().getName(), 
//									ei.getObjectRef(), 
//									fi.getName(), fieldMarks.orNull(), valueMarks.orNull(), frame.getMethodInfo().getBaseName(), ti.getPC().getLineNumber());	
//						}
//					}
//					
//					return super.execute(ti);
//				}
//			};
//		}

	//	@Override
	//	public Instruction getfield(String fieldName, String clsName, String fieldDescriptor) {
	//		return new GETFIELD(fieldName, clsName, fieldDescriptor);
	//	}

	//	@Override
	//	public Instruction return_() {
	//		return new RETURN();
	//	}

	//	@Override
	//	public Instruction ireturn() {
	//		return new IRETURN();
	//	}
	//
	//	@Override
	//	public Instruction dreturn() {
	//		return new DRETURN();
	//	}
	//
	//	@Override
	//	public Instruction freturn() {
	//		return new FRETURN();
	//	}
	//
	//	@Override
	//	public Instruction lreturn() {
	//		return new LRETURN();
	//	}
	//
	//	@Override
	//	public Instruction areturn() {
	//		return new ARETURN();
	//	}

	@Override
	public Instruction aastore() {
		return new AASTORE();
	}

//	@Override
//	public Instruction iastore() {
//		return new IASTORE();
//	}
//
//	@Override
//	public Instruction fastore() {
//		return new FASTORE();
//	}
//
//	@Override
//	public Instruction lastore() {
//		return new LASTORE();
//	}
//
//	@Override
//	public Instruction dastore() {
//		return new DASTORE();
//	}
	//
	//	@Override
	//	public Instruction ldc(int v) {
	//		return new LDC(v);
	//	}
	//
	//	@Override
	//	public Instruction ldc(float v) {
	//		return new LDC(v);
	//	}
	//
	//	@Override
	//	public Instruction ldc(String v, boolean isClass) {
	//		return new LDC(v, isClass);
	//	}

	//	@Override
	//	public Instruction aload(int localVarIndex) {
	//		return new ALOAD(localVarIndex);
	//	}
	//
	//	@Override
	//	public Instruction aload_0() {
	//		return new ALOAD(0);
	//	}
	////
	//	@Override
	//	public Instruction aload_1() {
	//		return new ALOAD(1);
	//	}
	////
	//	@Override
	//	public Instruction aload_2() {
	//		return new ALOAD(2);
	//	}
	////
	//	@Override
	//	public Instruction aload_3() {
	//		return new ALOAD(3);
	//	}

	//	 @Override
	//	 public Instruction invokestatic(String clsDescriptor, String methodName, String signature) {
	//	 return new INVOKESTATIC(clsDescriptor, methodName, signature);
	//	 }

	//	 @Override
	//	 public Instruction invokespecial(String clsDescriptor, String methodName, String signature) {
	//	 return new INVOKESPECIAL(clsDescriptor, methodName, signature);
	//	 }
	//
	// @Override
	// public Instruction invokevirtual(String clsDescriptor, String methodName, String signature) {
	// return new INVOKEVIRTUAL(clsDescriptor, methodName, signature);
	// }
}
