package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class LNEG extends gov.nasa.jpf.jvm.bytecode.LNEG {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long value = frame.popLong();

		frame.pushLong(-value);
		frame.setLongOperandAttr(marks.orNull());

		return getNext(ti);
	}

}
