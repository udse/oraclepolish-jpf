package testassist.bytecode.arithmetic;

import testassist.mark.Marks;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class LAND extends gov.nasa.jpf.jvm.bytecode.LAND {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();
		Optional<Marks> marks2 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long v2 = frame.popLong();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long v1 = frame.popLong();

		long r = v1 & v2;

		frame.pushLong(r);
		frame.setLongOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}
}
