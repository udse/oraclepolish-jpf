package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class FNEG extends gov.nasa.jpf.jvm.bytecode.FNEG {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		float value = frame.popFloat();

		frame.pushFloat(-value);
		frame.setOperandAttr(marks.orNull());

		return getNext(ti);
	}

}
