package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class DNEG extends gov.nasa.jpf.jvm.bytecode.DMUL {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		double v = frame.popDouble();

		frame.pushDouble(-v);

		frame.setLongOperandAttr(marks.orNull());

		return getNext(ti);
	}

}
