package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class INEG extends gov.nasa.jpf.jvm.bytecode.INEG {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		int value = frame.pop();

		frame.push(-value);
		frame.setOperandAttr(marks.orNull());

		return getNext(ti);
	}

}
