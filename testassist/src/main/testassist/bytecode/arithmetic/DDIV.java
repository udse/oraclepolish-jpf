package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class DDIV extends gov.nasa.jpf.jvm.bytecode.DDIV {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks2 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		double value2 = frame.popDouble();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		double value1 = frame.popDouble();

		frame.pushDouble(value1 / value2);
		frame.setLongOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}

}
