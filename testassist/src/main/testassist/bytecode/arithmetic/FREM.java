package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class FREM extends gov.nasa.jpf.jvm.bytecode.FREM {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks2 = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		float value2 = frame.popFloat();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		float value1 = frame.popFloat();

		frame.pushFloat(value1 % value2);
		frame.setOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}

}
