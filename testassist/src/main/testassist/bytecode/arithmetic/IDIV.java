package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class IDIV extends gov.nasa.jpf.jvm.bytecode.IDIV {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks2 = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		int value2 = frame.pop();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getOperandAttr(Marks.class));
		int value1 = frame.pop();

		if (0 == value2) {
			return ti.createAndThrowException("java.lang.ArithmeticException", "division by zero");
		}

		frame.push(value1 / value2);
		frame.setOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}

}
