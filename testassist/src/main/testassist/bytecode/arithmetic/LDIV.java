package testassist.bytecode.arithmetic;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class LDIV extends gov.nasa.jpf.jvm.bytecode.LDIV {

	@Override
	public Instruction execute(ThreadInfo ti) {

		StackFrame frame = ti.getTopFrame();

		Optional<Marks> marks2 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long value2 = frame.popLong();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		long value1 = frame.popLong();

		if (0 == value2) {
			return ti.createAndThrowException("java.lang.ArithmeticException", "division by zero");
		}

		frame.pushLong(value1 / value2);
		frame.setLongOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}

}
