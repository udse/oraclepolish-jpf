package testassist.bytecode.array;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

import testassist.mark.Marks;
import testassist.injector.Extractor;
import testassist.injector.Injector;
import testassist.util.Global;

public class ISTORE extends gov.nasa.jpf.jvm.bytecode.ISTORE {
	public ISTORE(int index) {
		super(index);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();

		Injector in = new Injector();
		in.taintTopOfOperandStack(frame, "istore : ");
		if (in.inTest(frame)) {
			Global.updateSet(in.generateTaintContent(frame, "istore : "));
		}
		frame.storeOperand(index);

		return getNext(ti);
	}

}
