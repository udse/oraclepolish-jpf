package testassist.bytecode.array;

import com.google.common.base.Optional;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ArrayIndexOutOfBoundsExecutiveException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class DASTORE extends gov.nasa.jpf.jvm.bytecode.DASTORE {

	@Override
	public Instruction execute(ThreadInfo ti) {
		int aref = peekArrayRef(ti); // need to be poly, could be LongArrayStore
		if (aref == -1) {
			return ti.createAndThrowException("java.lang.NullPointerException");
		}

		ElementInfo e = ti.getModifiableElementInfo(aref);

		if (isNewPorBoundary(e, ti)) {
			if (createAndSetArrayCG(e, ti, aref, peekIndex(ti), false)) {
				return this;
			}
		}

		int esize = getElementSize();
		StackFrame frame = ti.getModifiableTopFrame();

		Optional<Marks> attr = (esize == 1) ? Optional.fromNullable(frame.getOperandAttr(Marks.class))
				: Optional.fromNullable(frame.getLongOperandAttr(Marks.class));

		popValue(frame);
		index = frame.pop();
		// don't set 'arrayRef' before we do the CG check (would kill loop optimization)
		arrayRef = frame.pop();

		Instruction xInsn = checkArrayStoreException(ti, e);
		if (xInsn != null) {
			return xInsn;
		}

		try {
			setField(e, index);
			e.setObjectAttr(Marks.union(attr, Optional.fromNullable(e.getObjectAttr(Marks.class))));
			e.setElementAttrNoClone(index, attr.orNull()); // <2do> what if the value is the same but not the attr?
			return getNext(ti);

		} catch (ArrayIndexOutOfBoundsExecutiveException ex) { // at this point, the AIOBX is already processed
			return ex.getInstruction();
		}
	}
}
