package testassist.bytecode.array;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class ARRAYLENGTH extends gov.nasa.jpf.jvm.bytecode.ARRAYLENGTH {

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();

		arrayRef = frame.pop();

		if (arrayRef == -1) {
			return ti.createAndThrowException("java.lang.NullPointerException",
					"array length of null object");
		}


		ElementInfo ei = ti.getElementInfo(arrayRef);
		Marks ms = ei.getObjectAttr(Marks.class);

		frame.push(ei.arrayLength(), false);
		frame.setOperandAttr(ms);

		return getNext(ti);
	}
}
