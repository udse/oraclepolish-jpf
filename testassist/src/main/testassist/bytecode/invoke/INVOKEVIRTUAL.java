package testassist.bytecode.invoke;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.ThreadInfo;

import gov.nasa.jpf.vm.StackFrame;
import testassist.mark.Marks;
import testassist.mark.Mark;

import testassist.injector.*;

import gov.nasa.jpf.vm.Instruction;

public class INVOKEVIRTUAL extends gov.nasa.jpf.jvm.bytecode.INVOKEVIRTUAL {
	
	
	  public INVOKEVIRTUAL (String clsDescriptor, String methodName, String signature){
		    super(clsDescriptor, methodName, signature);
		  }
	@Override
	public Instruction execute(ThreadInfo ti) {
	    int objRef = ti.getCalleeThis(getArgSize());
	    
	    int argSize = getArgSize();
	    
	    Extractor e = new Extractor();
	    StackFrame f = ti.getTopFrame();
	    
	    Optional<Marks> taintsFromArgs = e.getTaints(f, argSize);

	    if (objRef == -1) {
	      lastObj = -1;
	      return ti.createAndThrowException("java.lang.NullPointerException", "Calling '" + mname + "' on null object");
	    }

	    MethodInfo callee = getInvokedMethod(ti, objRef);

	    if (callee == null) {
	      String clsName = ti.getClassInfo(objRef).getName();
	      return ti.createAndThrowException("java.lang.NoSuchMethodError", clsName + '.' + mname);
	    }
	    
	    ElementInfo ei = ti.getElementInfo(objRef);

	    if (callee.isSynchronized()) {
	      if (checkSyncCG(ei, ti)){
	        return this;
	      }
	    }

	    setupCallee( ti, callee); // this creates, initializes and pushes the callee StackFrame

	    return ti.getPC(); // we can't just return the first callee insn if a listener throws an exception
	}

}
