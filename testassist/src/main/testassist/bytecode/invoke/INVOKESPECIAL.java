package testassist.bytecode.invoke;

import com.google.common.base.Optional;

import testassist.mark.Marks;
import testassist.injector.Extractor;
import testassist.injector.Injector;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class INVOKESPECIAL extends gov.nasa.jpf.jvm.bytecode.INVOKESPECIAL {
	  
	  public INVOKESPECIAL (String clsDescriptor, String methodName, String signature){
		    super(clsDescriptor, methodName, signature);
		  }

	@Override
	  public Instruction execute (ThreadInfo ti) {
		    int argSize = getArgSize();
		    int objRef = ti.getCalleeThis( argSize);
		    lastObj = objRef;
		    
		    ///////
		    if (this.toString().contains("classMutants.Employee.SalariedEmployee")
		    ){
		    for (int i = 0; i < this.getArgSize(); i++){
		    	System.out.println("--invokespecial " + this + ti.getTopFrame().getOperandAttr(i, Marks.class));
		    }
		    }

		    ///////////
		    

		    // we don't have to check for NULL objects since this is either a ctor, a 
		    // private method, or a super method

		    // resolving the class referenced by InvokeSpecial
		    ClassInfo cls = ti.getTopFrameMethodInfo().getClassInfo();
		    try {
		      cls.resolveReferencedClass(cname);
		    } catch(LoadOnJPFRequired rre) {
		      return ti.getPC();
		    }

		    MethodInfo callee = getInvokedMethod(ti);

		    if (callee == null){
		      return ti.createAndThrowException("java.lang.NoSuchMethodException", "Calling " + cname + '.' + mname);
		    }

		    ElementInfo ei = ti.getElementInfo(objRef);

		    if (callee.isSynchronized()){
		      if (checkSyncCG(ei, ti)){
		        return this;
		      }
		    }

		    setupCallee( ti, callee); // this creates, initializes and pushes the callee StackFrame
		    

		    return ti.getPC(); // we can't just return the first callee insn if a listener throws an exception
		  }


}
