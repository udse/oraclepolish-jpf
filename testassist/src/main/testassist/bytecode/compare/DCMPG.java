package testassist.bytecode.compare;

import testassist.mark.Marks;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class DCMPG extends gov.nasa.jpf.jvm.bytecode.DCMPG {
	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();

		Optional<Marks> marks1 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		double v1 = frame.popDouble();
		Optional<Marks> marks2 = Optional.fromNullable(frame.getLongOperandAttr(Marks.class));
		double v2 = frame.popDouble();

		int condVal = conditionValue(v1, v2);

		frame.push(condVal);
		frame.setOperandAttr(Marks.union(marks2, marks1));

		return getNext(ti);
	}
}
