package testassist.bytecode.compare;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

public class INSTANCEOF extends gov.nasa.jpf.jvm.bytecode.INSTANCEOF {

	private String type;

	/**
	 * typeName is of a/b/C notation
	 */
	public INSTANCEOF(String typeName) {
		super(typeName);
		type = Types.getTypeSignature(typeName, false);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		if (Types.isReferenceSignature(type)) {
			String t;
			if (Types.isArray(type)) {
				// retrieve the component terminal
				t = Types.getComponentTerminal(type);
			} else {
				t = type;
			}

			// resolve the referenced class
			try {
				ti.resolveReferencedClass(t);
			} catch (LoadOnJPFRequired lre) {
				return ti.getPC();
			}
		}

		StackFrame frame = ti.getModifiableTopFrame();
		int objref = frame.pop();

		if (objref == -1) {
			frame.push(0);
		} else if (ti.getElementInfo(objref).instanceOf(type)) {
			frame.push(1);
		} else {
			frame.push(0);
		}

		if (objref != -1) {
			Marks attrs = new Marks();

			ElementInfo ei = ti.getElementInfo(objref);
			for (int i = 0; i < ei.getNumberOfFields(); i++) {
				FieldInfo fi = ei.getFieldInfo(i);
				Marks attr = ei.getFieldAttr(fi, Marks.class);
				if (attr != null) attrs.addAll(attr);
			}

			frame.setOperandAttr(attrs);
		}

		return getNext(ti);
	}

}
