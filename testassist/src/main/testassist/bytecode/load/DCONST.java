package testassist.bytecode.load;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;

import com.google.common.base.Optional;

public class DCONST extends gov.nasa.jpf.jvm.bytecode.DCONST {

	private Marks marks;

	public DCONST(double value) {
		super(value);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();
		frame.pushDouble(value);
		frame.setLongOperandAttr(marks);

		return getNext(ti);
	}

	public void setMarks(Optional<Marks> marks) {
		this.marks = marks.orNull();
	}

	@Override
	public void cleanupTransients() {
		marks = null;
	}

}
