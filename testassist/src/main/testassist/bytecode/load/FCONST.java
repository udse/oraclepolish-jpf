//
// Copyright (C) 2006 United States Government as represented by the
// Administrator of the National Aeronautics and Space Administration
// (NASA).  All Rights Reserved.
// 
// This software is distributed under the NASA Open Source Agreement
// (NOSA), version 1.3.  The NOSA has been approved by the Open Source
// Initiative.  See the file NOSA-1.3-JPF at the top of the distribution
// directory tree for the complete NOSA document.
// 
// THE SUBJECT SOFTWARE IS PROVIDED "AS IS" WITHOUT ANY WARRANTY OF ANY
// KIND, EITHER EXPRESSED, IMPLIED, OR STATUTORY, INCLUDING, BUT NOT
// LIMITED TO, ANY WARRANTY THAT THE SUBJECT SOFTWARE WILL CONFORM TO
// SPECIFICATIONS, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
// A PARTICULAR PURPOSE, OR FREEDOM FROM INFRINGEMENT, ANY WARRANTY THAT
// THE SUBJECT SOFTWARE WILL BE ERROR FREE, OR ANY WARRANTY THAT
// DOCUMENTATION, IF PROVIDED, WILL CONFORM TO THE SUBJECT SOFTWARE.
//
package testassist.bytecode.load;

import com.google.common.base.Optional;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

/**
 * Push float
 * ... => ..., <f>
 */
public class FCONST extends gov.nasa.jpf.jvm.bytecode.FCONST {

	private Marks marks;
	
	public FCONST(float value){
		super(value);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();

		frame.pushFloat(value);

		frame.setOperandAttr(marks);
		return getNext(ti);
	}

	public void setMarks(Optional<Marks> marks) {
		this.marks = marks.orNull();
	}

	@Override
	public void cleanupTransients() {
		marks = null;
	}
}
