package testassist.bytecode.load;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ArrayIndexOutOfBoundsExecutiveException;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class AALOAD extends gov.nasa.jpf.jvm.bytecode.AALOAD {
 @Override
  public Instruction execute (ThreadInfo ti) {
    StackFrame frame = ti.getModifiableTopFrame();

    // we need to get the object first, to check if it is shared
    int aref = frame.peek(1); // ..,arrayRef,idx
    if (aref == -1) {
      return ti.createAndThrowException("java.lang.NullPointerException");
    }
    ElementInfo e = ti.getElementInfo(aref);

    if (isNewPorBoundary(e, ti)) {
      if (createAndSetArrayCG(e,ti, aref,peekIndex(ti),true)) {
        return this;
      }
    }
    
    Marks ms = frame.getOperandAttr(Marks.class);
    index = frame.pop();

    // we should not set 'arrayRef' before the CG check
    // (this would kill the CG loop optimization)
    arrayRef = frame.pop();
    
    try {
      push(frame, e, index);

      Object attr = e.getElementAttr(index);
      if (attr != null) {
        if (getElementSize() == 1) {
          frame.setOperandAttr(attr);
        } else {
          frame.setLongOperandAttr(attr);
        }
      }
      
      return getNext(ti);
    } catch (ArrayIndexOutOfBoundsExecutiveException ex) {
      return ex.getInstruction();
    }
  }
}
