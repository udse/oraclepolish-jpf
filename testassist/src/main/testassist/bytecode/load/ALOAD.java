package testassist.bytecode.load;

import testassist.mark.Marks;
import testassist.injector.Injector;
import testassist.util.Global;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class ALOAD extends gov.nasa.jpf.jvm.bytecode.ALOAD {
	public ALOAD(int index) {
		super(index);
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();
		int local = frame.getLocalVariable(index);
		frame.pushRef(local);
		Marks ms = frame.getLocalAttr(index, Marks.class);
		if (ms != null) System.out.println("***" + ms);
		ElementInfo ei = ti.getHeap().get(local);
		frame.setOperandAttr(ms);

		return getNext(ti);
	}

}
