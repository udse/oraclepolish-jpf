package testassist.bytecode.load;

import testassist.injector.Injector;
import testassist.util.Global;
import gov.nasa.jpf.jvm.bytecode.LDC.Type;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.LoadOnJPFRequired;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import gov.nasa.jpf.vm.Types;

public class LDC extends gov.nasa.jpf.jvm.bytecode.LDC {
	
	public LDC() {}

	  public LDC (String s, boolean isClass){
		  super(s, isClass);
	  }

	  public LDC (int v){
		  super(v);
	  }

	  public LDC (float f){
		  super(f);
	  }
	  @Override
	  public Instruction execute (ThreadInfo ti) {
	    StackFrame frame = ti.getModifiableTopFrame();
	    Injector in = new Injector();
	    
	    
	    switch (getType()){
	      case STRING:
	        // too bad we can't cache it, since location might change between different paths
	        ElementInfo eiValue = ti.getHeap().newInternString(string, ti); 
	        value = eiValue.getObjectRef();
	        frame.pushRef(value);
	        break;

	      case INT:
	      case FLOAT:
	        frame.push(value);
	        break;

	      case CLASS:
	        ClassInfo ci;
	        // resolve the referenced class
	        try {
	          ClassInfo cls = ti.getTopFrameMethodInfo().getClassInfo();
	          ci = cls.resolveReferencedClass(string);
	        } catch(LoadOnJPFRequired lre) {
	          return frame.getPC();
	        }

	        // LDC doesn't cause a <clinit> - we only register all required classes
	        // to make sure we have class objects. <clinit>s are called prior to
	        // GET/PUT or INVOKE
	        if (!ci.isRegistered()) {
	          ci.registerClass(ti);
	        }

	        frame.pushRef( ci.getClassObjectRef());

	        break;
	    }
	    
	    in.taintTopOfOperandStack(frame, "ldc : ");
		if (in.inTest(frame)){
	    Global.updateSet(in.generateTaintContent(frame, "ldc : "));
		}
	    
	    return getNext(ti);
	  }
}
