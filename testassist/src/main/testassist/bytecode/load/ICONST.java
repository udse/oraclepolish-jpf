package testassist.bytecode.load;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Mark;
import testassist.mark.Marks;
import testassist.listener.MyListener;

import com.google.common.base.Optional;

public class ICONST extends gov.nasa.jpf.jvm.bytecode.ICONST {

	private Marks marks;

	public ICONST(int value) {
		super(value);

	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getTopFrame();

		frame.push(value);
//		if (frame.getMethodName().contains("count"))
//			frame.setOperandAttr(new Marks(new Mark("iconst : " + this.getPosition())));

		return getNext(ti);
	}

	public void setMarks(Optional<Marks> marks) {
		this.marks = marks.orNull();
	}

	@Override
	public void cleanupTransients() {
		marks = null;
	}

}
