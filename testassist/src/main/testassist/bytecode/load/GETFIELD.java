package testassist.bytecode.load;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class GETFIELD extends gov.nasa.jpf.jvm.bytecode.GETFIELD {

	public GETFIELD(String fieldName, String classType, String fieldDescriptor) {
		super(fieldName, classType, fieldDescriptor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
		StackFrame frame = ti.getModifiableTopFrame();

		int objRef = frame.peek(); // don't pop yet, we might re-enter
		lastThis = objRef;
		if (objRef == -1) {
			return ti.createAndThrowException("java.lang.NullPointerException",
					"referencing field '" + fname + "' on null object");
		}

		ElementInfo ei = ti.getElementInfo(objRef);

		FieldInfo fi = getFieldInfo();
		if (fi == null) {
			return ti.createAndThrowException("java.lang.NoSuchFieldError",
					"referencing field '" + fname + "' in " + ei);
		}

		// check if this breaks the current transition
		if (isNewPorFieldBoundary(ti, fi, objRef)) {
			if (createAndSetSharedFieldAccessCG(ei, ti)) {
				return this;
			}
		}

		frame.pop(); // Ok, now we can remove the object ref from the stack
		Marks attr = ei.getFieldAttr(fi, Marks.class);

		// We could encapsulate the push in ElementInfo, but not the GET, so we keep it at a similiar level
		if (fi.getStorageSize() == 1) { // 1 slotter
			int ival = ei.get1SlotField(fi);
			lastValue = ival;

			if (fi.isReference()) {
				frame.pushRef(ival);

			} else {
				frame.push(ival);
			}

			frame.setOperandAttr(attr);

		} else { // 2 slotter
			long lval = ei.get2SlotField(fi);
			lastValue = lval;

			frame.pushLong(lval);
			frame.setLongOperandAttr(attr);
		}

		return getNext(ti);
	}
}
