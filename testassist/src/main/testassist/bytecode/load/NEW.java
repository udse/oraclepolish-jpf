package testassist.bytecode.load;

import com.google.common.base.Optional;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;
import testassist.mark.Marks;
import testassist.injector.Injector;
import gov.nasa.jpf.vm.*;

public class NEW extends gov.nasa.jpf.jvm.bytecode.NEW {
	private ClassInfo _ci;
	private Optional<Marks> marks; 

	public NEW(String clsDescriptor) {
		super(clsDescriptor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Instruction execute(ThreadInfo ti) {
	    Heap heap = ti.getHeap();
	    ClassInfo ci;
	
	    ClassInfo cls = ti.getTopFrameMethodInfo().getClassInfo();
	    try {
	      ci = cls.resolveReferencedClass(cname);
	    } catch(LoadOnJPFRequired lre) {
	      return ti.getPC();
	    }
	
	    if (!ci.isRegistered()){
	      ci.registerClass(ti);
	    }
	
	    if (!ci.isInitialized()) {
	      if (ci.initializeClass(ti)) {
	        return ti.getPC();
	      }
	    }
	
	    if (heap.isOutOfMemory()) {
	      return ti.createAndThrowException("java.lang.OutOfMemoryError",
	                                        "trying to allocate new " + cname);
	    }
	
	    ElementInfo ei = heap.newObject(ci, ti);
	    int objRef = ei.getObjectRef();
	    newObjRef = objRef;
//	    
//	    if (ci.getName().contains("Human")) {
//	    	System.out.println("new : " + objRef);
//	    }
	
	    StackFrame frame = ti.getModifiableTopFrame();
	    frame.pushRef( objRef);
		_ci = ci;
		
		//********************
		frame.setOperandAttr(marks);
		//********************
	
	    return getNext(ti);	
	}

	public void setMarks(Optional<Marks> marks) {
		this.marks = marks;
	}
}
