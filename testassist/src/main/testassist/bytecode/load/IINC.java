package testassist.bytecode.load;

import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class IINC extends gov.nasa.jpf.jvm.bytecode.IINC {

	public IINC(int localVarIndex, int increment) {
		super(localVarIndex, increment);
		// TODO Auto-generated constructor stub
	}
	@Override
	public Instruction execute (ThreadInfo ti) {
	  StackFrame frame = ti.getModifiableTopFrame();
	  
	  int v = frame.getLocalVariable(index);
	  v += increment;
	  
	  frame.setLocalVariable(index, v, false);

		return getNext(ti);
	}
}
