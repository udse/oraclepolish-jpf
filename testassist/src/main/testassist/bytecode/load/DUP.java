package testassist.bytecode.load;

import testassist.mark.Marks;
import gov.nasa.jpf.vm.Instruction;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.ThreadInfo;

public class DUP extends gov.nasa.jpf.jvm.bytecode.DUP {

	@Override
	  public Instruction execute (ThreadInfo th) {
		    StackFrame frame = th.getModifiableTopFrame();
		    Marks ms = frame.getOperandAttr(Marks.class);
		    
		    frame.dup();
		    frame.setOperandAttr(ms);
		    Marks mss = frame.getOperandAttr(Marks.class);

		    return getNext(th);
		  }
}