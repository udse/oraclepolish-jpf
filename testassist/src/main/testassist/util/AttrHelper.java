package testassist.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import com.google.common.collect.Sets;

import gov.nasa.jpf.vm.StackFrame;
import testassist.mark.Marks;

public class AttrHelper {

	private Pair<Map<Integer, Integer>, Stack<Pair<Integer, Marks>>> emptyAttr =
			new Pair<Map<Integer, Integer>, Stack<Pair<Integer, Marks>>>
			(new HashMap<Integer, Integer>(), new Stack<Pair<Integer, Marks>>());

	public Marks unionOnStack(StackFrame sf) {
		return h_unionOMarks(sf, null);
	}

	private Marks h_unionOMarks(StackFrame sf, Marks acc) {
		if (sf == null)
			return acc;
		else {
			Marks local = Marks.unionIter(getStack(sf).iterator());
			acc = Marks.union(acc, local);
			return h_unionOMarks(sf.getPrevious(), acc);
		}
	}

	//	public Set<Marks> marksOnStack(StackFrame sf) {
	//		return Sets.newHashSet(getStack(sf).iterator());
	//	}

	private Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> getSFInfo(StackFrame sf) {
		//		if (currentFrame == null) {
		//			System.out.println("null : " + ti.getTopFrameMethodInfo());
		//			return emptyAttr;
		//		}
		Pair attr = sf.getFrameAttr(Pair.class);
		return (attr == null ? emptyAttr : attr);
	}

	public void setSFInfo(StackFrame sf, Map<Integer, List<Integer>> map) {
		Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> attr =
				new Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>>(map, new Stack<Pair<Integer, Marks>>());
		sf.setFrameAttr(attr);
	}

	public Stack<Pair<Integer, Marks>> getStack(StackFrame sf) {
		Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> attr = getSFInfo(sf);
		Stack<Pair<Integer, Marks>> currentStack = attr.getRight();
		return currentStack;
	}

	public Map<Integer, List<Integer>> getMap(StackFrame sf) {
		Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> attr = getSFInfo(sf);
		Map<Integer, List<Integer>> currentMap = attr.getLeft();
		return currentMap;
	}

	public void PUSH(StackFrame sf, Marks ms, Integer branch) {
		Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> attr = getSFInfo(sf);
		Stack<Pair<Integer, Marks>> currentStack = attr.getRight();
		currentStack.push(new Pair<>(branch, ms));
	}

	public void POP(StackFrame sf, int bco) {
		StackFrame caller = sf.getPrevious();

		Pair<Map<Integer, List<Integer>>, Stack<Pair<Integer, Marks>>> attr 
																	= getSFInfo(sf);
		Map<Integer, List<Integer>> currentMap = attr.getLeft();
		Stack<Pair<Integer, Marks>> currentStack = attr.getRight();
		if (currentMap.isEmpty()) return;
		List<Integer> branches = currentMap.get(bco);

		if (branches == null) return;

		while (!currentStack.isEmpty() &&
				branches.contains(currentStack.peek().getLeft()))
		{
			if (sf.getMethodInfo().toString().contains("EqualsBuilder.append(Ljava")) {
				System.out.println("pop " + bco + branches + currentStack);
			}
			currentStack.pop();
		}
	}
}
