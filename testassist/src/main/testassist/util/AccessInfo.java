package testassist.util;

public class AccessInfo {
	
	String method;
	int total;
	int tainted;

	public AccessInfo(String method, int total, int tainted){
		this.method = method;
		this.total = total;
		this.tainted = tainted;
	}
	
	public void incTainted(){
		this.tainted++;
		this.total++;
	}
	
	public void incUntainted(){
		this.total++;
	}
	
	@Override
	public boolean equals(Object o){
		if (o instanceof AccessInfo){
			AccessInfo ai = (AccessInfo) o;
			return this.method.equals(ai.method);
		}
		return false;
	}
	
	@Override
	public String toString(){
		return this.method + " : " + this.tainted + "/" + this.total;
	}
}
