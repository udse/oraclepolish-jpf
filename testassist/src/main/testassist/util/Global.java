package testassist.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;

import com.google.common.base.Predicate;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.sun.jdi.ObjectReference;
import com.sun.jdi.ReferenceType;

import testassist.mark.ControlledMark;
import testassist.mark.Mark;
import testassist.mark.Marks;
import edu.udel.testassist.configuration.MethodIdentifier;
import edu.udel.testassist.configuration.TestInfo;

public class Global {
	public static final List<String> EXCLUDE_PREFIXES = ImmutableList.of(
			"$",
			"java.",
			"javax.",
			"com.sun.",
			"sun.",
			"org.omg.",
			"junit.",
			"org.junit.",
			"com.apple.",
			"com.intellij.",
			"org.netbeans.",
			"edu.udel.",
			"testassist.",
			"com.google."
			);

	public static boolean inOracle;
	public static TestInfo testInfo = null;
	public static boolean last = false;

	public static MethodIdentifier identifier;
	public static Multimap<ReferenceType, ObjectReference> testObjectsByType;
	public static Multimap<ElementInfo, FieldInfo> modifications;
	public static List<ElementInfo> exceptions;
	public static Set<Mark> taintSet = new HashSet<Mark>();
	public static Set<Mark> missedbyifs = new HashSet<Mark>();
	public static Marks nativeTaint = null;
	public static boolean justExited = false;
	public static int nativeSize = 0;
	public static int recordedobj = -1;
	public static int testresult = -1;
	public static int MUT = 0;

	public static boolean taintIt;

	//	public static TaintInfo taintInfo;

	public static boolean toExclude(List<String> lst, String cn) {
		for (String s : lst) {
			if (cn.startsWith(s)) {
				return true;
			}
		}
		return false;
	}

	public static void reset(TestInfo test) {
		testresult = -1;
		testInfo = test;
		inOracle = false;
		justExited = false;
		identifier = testInfo.identifier();
		testObjectsByType = ArrayListMultimap.create();
		modifications = ArrayListMultimap.create();
		exceptions = new ArrayList<ElementInfo>();
		nativeTaint = null;
		nativeSize = 0;
		taintSet.clear();
		missedbyifs.clear();
	}

	public static boolean checkInTest(String left, String right) {
		return left.equals(right);
	}

	public static void summarize() {

	}

	public static void updateSet(String generateTaintContent) {
		taintSet.addAll(new Marks(new ControlledMark(generateTaintContent)));
		
	}

}
