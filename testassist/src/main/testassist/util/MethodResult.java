package testassist.util;

import java.util.HashSet;
import java.util.Set;

import testassist.mark.Mark;
import testassist.mark.Marks;

public class MethodResult {
	private int tainted_fields = 0;
	private int fields_accessed = 0;
	private int excep_thrown = 0;
	private int tainted_excep = 0;
	
	private Set<Mark> freeTaints;
	
	public MethodResult() {
		tainted_fields = 0;
		fields_accessed = 0;
		excep_thrown = 0;
		tainted_excep = 0;
		freeTaints = new HashSet<>();
	}
	
	public MethodResult(int a, int b, int c, int d){
		tainted_fields = a;
		fields_accessed = b;
		excep_thrown = c;
		tainted_excep = d;
		freeTaints = new HashSet<>();
	}
	
	synchronized public void gotTainted(Marks ms){
		tainted_fields++;
		freeTaints.removeAll(ms);
	}
	
	synchronized public void addTaints(Marks ms){
		freeTaints.addAll(ms);
	}
	
	synchronized public void accessField(){
		fields_accessed++;
	}
	
	
	public synchronized MethodResult add(MethodResult that){
		return new MethodResult(this.tainted_fields + that.tainted_fields,
									this.fields_accessed + that.fields_accessed,
									this.excep_thrown + that.excep_thrown,
									this.tainted_excep + that.tainted_excep
									);
	}

	@Override
	public String toString(){
		return String.format("%s, %s, %s, %s, \nleftOvers:\n%s", 
					tainted_fields, fields_accessed, tainted_excep, excep_thrown, freeTaints);
	}

	public String toCSV(){
		return String.format("%s, %s, %s, %s", 
					tainted_fields, fields_accessed, tainted_excep, excep_thrown);
	}
}
