package testassist.configuration;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;



class Location {
	int offset;
	int size;
	
	@JsonCreator
	public Location(@JsonProperty("offset") int offset, @JsonProperty("size") int size){
		this.offset=offset;
		this.size=size;
	}
	
	public String toString(){
		return String.format("[%s,%s]", offset, size);
	}
}