package testassist;

import edu.udel.testassist.configuration.Configuration;
import edu.udel.testassist.configuration.TestInfo;
import gov.nasa.jpf.Config;
import gov.nasa.jpf.Error;
import gov.nasa.jpf.JPF;
import gov.nasa.jpf.search.PathSearch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import testassist.bytecode.InstructionFactory;
import testassist.listener.ClassLoadingListener;
import testassist.listener.ControlFlowListener;
import testassist.listener.ControlledMarkInjector;
import testassist.listener.ControlledMutantInjector;
import testassist.listener.NativeMethodListener;
import testassist.listener.PreemptedClassLoading;
import testassist.listener.TaintMarkChecker;
import testassist.listener.UncontrolledMarkInjector;
import testassist.listener.UncontrolledMutantInjector;
import testassist.mark.Mark;
import testassist.mark.Marks;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

public class Main {

	private static PrintWriter nojpf;
	private static PrintWriter writer;
	public static int mut;

	public static void main(String[] args) throws IOException, InterruptedException {

		//mut = Integer.parseInt(args[0]);
		mut = 0;
		String toKeep = "";

		final Predicate<Error> isFailure = new Predicate<Error>() {
			public boolean apply(Error e) {
				if (e == null) return false;
				if (e.getDetails() == null) return false;
				return e.getDetails().contains("TestFailureException");
			}
		};

		final Predicate<Error> isSuccess = new Predicate<Error>() {
			public boolean apply(Error e) {
				if (e == null) return false;
				if (e.getDetails() == null) return false;
				return e.getDetails().contains("TestSuccessException");
			}
		};

		OptionParser optionParser = new OptionParser();

		final OptionSpec<File> configurationOption = optionParser.accepts("configuration")
				.withRequiredArg()
				.ofType(File.class)
				.required();

		final OptionSpec<File> outputDirectoryOption = optionParser.accepts("output")
				.withRequiredArg()
				.ofType(File.class)
				.defaultsTo(new File(System.getProperty("user.dir")));

		final OptionSpec<Integer> timeoutOption = optionParser.accepts("timeout")
				.withRequiredArg()
				.ofType(Integer.class)
				.defaultsTo(2);

		final OptionSet options = optionParser.parse(args);

		final File outputDirectory = options.valueOf(outputDirectoryOption);
		outputDirectory.mkdirs();

		ObjectMapper mapper = new ObjectMapper();
		Configuration configuration = mapper.readValue(options.valueOf(configurationOption), Configuration.class);

		final String projpath = "/home/huoc/Projects/java/oraclepolish-jpf";

		final String classpath = Joiner.on(File.pathSeparator).join(new ImmutableList.Builder<String>()
				.addAll(configuration.classpath())
				.add(projpath + "/build")
				.add(projpath+"/lib/jackson-annotations-2.2.2.jar")
				.add(projpath+"/lib/persistence-api-1.0.jar")
				//			.add(projpath+"/jpf-core/build/jpf.jar")
				.build());

		//List<TestInfo> tests = new ArrayList<TestInfo>();
		//tests.add(configuration.tests().get(1));
		List<TestInfo> tests = configuration.tests();
		System.out.println(tests);

		writer = new PrintWriter(new BufferedWriter(new FileWriter(projpath + "/jpfresult.csv", true)));
		PrintWriter nolistener = new PrintWriter(new BufferedWriter(new FileWriter(projpath + "/nolistenerresult.csv", true)));
		PrintWriter truePositiveRecord = new PrintWriter(new BufferedWriter(new FileWriter(projpath + "/output/" + tests.get(0).className())));
		PrintWriter positiveRecord = new PrintWriter(new BufferedWriter(new FileWriter(projpath + "/positives/" + tests.get(0).className())));
		nojpf = new PrintWriter(new BufferedWriter(new FileWriter(projpath + "/nojpfresult.csv", true)));

		List<TestInfo> safe = new ArrayList<>();
		Map<TestInfo, Set<String>> loadMap = new HashMap<TestInfo, Set<String>>();

		int JPF_SIZE = 8;

		for (TestInfo test : tests) {
			ExecutorService executor = Executors.newSingleThreadExecutor();

			try {
				//				if (test.ranges().isEmpty()) {
				//					throw new RuntimeException("\n\n" + i + ") " + test.identifier() + "\nNO assertion");
				//				}
				if (!test.identifier().toString().contains(toKeep)) {
					continue;
				}
				if (test.ranges().isEmpty()) {
					writer.printf(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
					throw new RuntimeException("\n\n" + test.identifier() + "\nNO assertion");
				}
				long start = System.currentTimeMillis();

				ImmutableList<String> inferiorArgs = ImmutableList.<String> builder()
						.add("+classpath=" + classpath)
						.add(SingleTestRunner.class.getName())
						//				.add(JPFTask.class.getName())
						.add(test.identifier().toString())
						.build();

				String[] jpfArgs = new String[inferiorArgs.size()];
				jpfArgs = inferiorArgs.toArray(jpfArgs);

				Config config = new Config(jpfArgs);
				config.setProperty("native_classpath", "/home/huoc/Projects/java/oraclepolish-jpf/build");
				config.setProperty("vm.peer.packages", "testassist.annotation,<model>,gov.nasa.jpf.vm,<default>");
				config.setProperty("vm.no_orphan_methods", "false");
				config.setProperty("vm.por", "false");
				config.setProperty("vm.storage.class", null);
				config.setProperty("search.class", PathSearch.class.getName());
				config.setProperty("search.properties", "gov.nasa.jpf.vm.NotDeadlockedProperty,gov.nasa.jpf.vm.NoUncaughtExceptionsProperty");
				config.setProperty("jvm.insn_factory.class", InstructionFactory.class.getName());

				ClassLoadingListener cll = new ClassLoadingListener(test);

				JPF testDriver = new JPF(config);
				testDriver.addListener(cll);

				Future<Void> dfuture = (Future<Void>) executor.submit(testDriver);

				//				System.out.println(dfuture.get(options.valueOf(timeoutOption) / 5, TimeUnit.SECONDS));
				System.out.println(dfuture.get(20 / 5, TimeUnit.SECONDS));

				List<Error> derrors = testDriver.getReporter().getErrors();

				if (Iterables.any(derrors, isFailure)) {
					writer.printf(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
					long stop = System.currentTimeMillis();
					nolistener.printf(test.identifier() + ", -1, -1, -1, -1, 1, -1, -1," + ((stop - start) + 1) + "\n");
//					loadMap.put(test, cll.getLateLoaded());
					//					safe.add(test);
					//					safe.add(test);
				}
				else if (Iterables.any(derrors, isSuccess)) {
					System.out.println("succeed");
					safe.add(test);
					loadMap.put(test, cll.getLateLoaded());

					long stop = System.currentTimeMillis();
					nolistener.printf(test.identifier() + ", -1, -1, -1, -1, 1, -1, -1," + ((stop - start) + 1) + "\n");
				}
				else {
					System.out.println("FAIL!");
					for (Error e : derrors) {
						System.out.println("Error : " + e.getDescription());
					}
					writer.printf(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
					//					executor.shutdownNow();
					//					throw new RuntimeException("app excep");
				}
			} catch (ExecutionException | TimeoutException e1) {
				e1.printStackTrace();
				writer.printf(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
				writer.printf(test.identifier() + ", -1, -1, -1, -1, -1, -1, -1, 0\n");
			} finally {
				executor.shutdownNow();
			}
		}
		nolistener.close();

		//			for (int i = options.valueOf(startOption); i < safe.size()/2; i++) {
		for (int i = 0; i < safe.size(); i++) {

			final TestInfo test = safe.get(i);
			long start = System.currentTimeMillis();
			StringBuilder sb = new StringBuilder();

			if (!test.identifier().toString().contains(toKeep)) {
				continue;
			}

			TaintMarkChecker tmc = new TaintMarkChecker(test);

			ExecutorService executor = Executors.newSingleThreadExecutor();
			ExecutorService mexecutor = Executors.newSingleThreadExecutor();
			ExecutorService texecutor = Executors.newSingleThreadExecutor();

			try {

				ImmutableList<String> inferiorArgs = ImmutableList.<String> builder()
						.add("+classpath=" + classpath)
						.add(SingleTestRunner.class.getName())
						//				.add(JPFTask.class.getName())
						.add(test.identifier().toString())
						.build();

				String[] jpfArgs = new String[inferiorArgs.size()];
				jpfArgs = inferiorArgs.toArray(jpfArgs);

				Config config = new Config(jpfArgs);
				config.setProperty("native_classpath", "/home/huoc/Projects/java/oraclepolish-jpf/build");
				config.setProperty("vm.peer.packages", "testassist.annotation,<model>,gov.nasa.jpf.vm,<default>");
				config.setProperty("vm.no_orphan_methods", "false");
				config.setProperty("vm.por", "false");
				config.setProperty("vm.storage.class", null);
				config.setProperty("search.class", gov.nasa.jpf.search.PathSearch.class.getName());
				config.setProperty("search.properties", "gov.nasa.jpf.vm.NotDeadlockedProperty,gov.nasa.jpf.vm.NoUncaughtExceptionsProperty");
				config.setProperty("jvm.insn_factory.class", InstructionFactory.class.getName());

				JPF jpf = new JPF(config);

				EntityManagerFactory emf =
						Persistence.createEntityManagerFactory(
								"/home/huoc/Projects/testassist/database/controlflow.odb");
				EntityManager em = emf.createEntityManager();

				ControlFlowListener cfl = new ControlFlowListener(em, test);
				ControlledMarkInjector controlledMarkInjector = new ControlledMarkInjector(test);
//				AutoControlledMutantInjector acontrolledMutator = new AutoControlledMutantInjector(ti, )
				UncontrolledMarkInjector uncontrolledMarkInjector = new UncontrolledMarkInjector(test);
				PreemptedClassLoading pcl = new PreemptedClassLoading(loadMap.get(test));

				/////////////////

				///////////////////

				System.out.printf("%d) %s ... ", i, test.identifier());
				System.out.flush();

				System.out.println("JPF Started..");
				//					System.out.println(future.get(options.valueOf(timeoutOption), TimeUnit.SECONDS));

				jpf.addListener(pcl);
				jpf.addListener(uncontrolledMarkInjector);
				jpf.addListener(controlledMarkInjector);
				jpf.addListener(new NativeMethodListener(test));
				jpf.addListener(cfl);
				jpf.addListener(tmc);
				Future<?> future = executor.submit(jpf);
				System.out.println(future.get(20, TimeUnit.SECONDS));
				System.out.println("\nFROM tmc : \n" + Joiner.on("\n\n").join(tmc.getComputationMarks()));
				System.out.println("\nFROM tmc : \n" + Joiner.on("\n\n").join(tmc.getStructuralMarks()));

				System.out.println("Finished!");

				List<Error> errors = jpf.getReporter().getErrors();
				em.close();

				if (Iterables.any(errors, isFailure)) {
					sb.append(String.format("%s, %s, %s, 0", test.identifier(), tmc.toCSV(), uncontrolledMarkInjector.sizeOfMarks()));
				}
				else if (Iterables.any(errors, isSuccess)) {
					sb.append(String.format("%s, %s, %s, 1", test.identifier(), tmc.toCSV(), uncontrolledMarkInjector.sizeOfMarks()));

					if (tmc.getComputationMarks().getSizeOfUncontrolledMarks() > 0) {
						positiveRecord.printf("\n%s\n\t%s", test.identifier(),
								Joiner.on("\n\t").join(tmc.getComputationMarks().getUncontrolledMarks()));
					}

				}
				else {
					System.out.println("FAIL!");
					sb.append(String.format("%s, %s, %s, -1", test.identifier(), tmc.toCSV(), uncontrolledMarkInjector.sizeOfMarks()));
					//					executor.shutdownNow();
					//					throw new RuntimeException("app excep");
				}

				//				}

				Marks uncontrolled = tmc.getComputationMarks().getUncontrolledMarks();

				//				boolean isTp = false;
				Marks tps = new Marks();

				if (uncontrolled.isEmpty()) {
					sb.append(", 0, 1");
				}
				else {
					for (Mark m : uncontrolled) {
						try {
							JPF mutater = new JPF(config);
							UncontrolledMutantInjector minjector = new UncontrolledMutantInjector(test, new Marks(m));

							PreemptedClassLoading mpcl = new PreemptedClassLoading(loadMap.get(test));
							mutater.addListener(minjector);
							mutater.addListener(mpcl);

							Future<?> mfuture = mexecutor.submit(mutater);

							System.out.println("\nMUTATION started");
							System.out.println(mfuture.get(options.valueOf(timeoutOption), TimeUnit.SECONDS));
							System.out.println("MUTATION finished!");

							List<Error> merrors = mutater.getReporter().getErrors();

							if (Iterables.any(merrors, isFailure)) {
								tps.add(m);
							}
							else if (Iterables.any(merrors, isSuccess)) {
							}
							else {
								System.out.println("MUTATION FAIL!");
								tps.add(m);
								//							sb.append(", -1");
							}

						} catch (Exception e1) {
							System.out.println(e1);
							//						sb.append(", -1");
							e1.printStackTrace();
						}
					}

					if (tps.isEmpty()) {
						sb.append(", 0, 1");
					}
					else {
						sb.append(", " + tps.size() + ", 0");
						truePositiveRecord.printf(String.format("%s\n%s\n", test.className() + "." + test.methodName(),
								Joiner.on("\t\n").join(tps)));
					}
				}

				JPF testInputMutator = new JPF(config);
				Set<Mark> contributors = Sets.union(tmc.getComputationMarks().getControlledMarks(), tmc.getStructuralMarks());
				Set<Mark> leftovers = Sets.difference(controlledMarkInjector.getAllTestMarks(), contributors);
				System.out.println("leftovers : " + leftovers);
				ControlledMutantInjector tmi = new ControlledMutantInjector(test, leftovers);
//				AutoControlledMutantInjector acontrolledMutator = new AutoControlledMutantInjector(test, leftovers);
				testInputMutator.addListener(tmi);
				Future<?> tfuture = texecutor.submit(testInputMutator);

				System.out.println("\ntest MUTATION started");
				System.out.println(tfuture.get(options.valueOf(timeoutOption), TimeUnit.SECONDS));
				System.out.println("test MUTATION finished!");

				List<Error> terrors = testInputMutator.getReporter().getErrors();

				if (Iterables.any(terrors, isFailure)) {
					sb.append(", 0");
				}
				else if (Iterables.any(terrors, isSuccess)) {
					sb.append(", 1");

				}
				else {
					System.out.println("MUTATION FAIL!");
					sb.append(", -1");
					//					throw new RuntimeException("app excep");
				}

			} catch (Exception e) {
				String[] s = sb.toString().split(",");

				if (s.length == 1) {
					System.out.println(sb);
					sb.append(test.identifier());
				}

				for (int j = s.length; j < JPF_SIZE; j++) {
					sb.append(", -1");
				}

				System.out.println("\n\n" + tmc.getComputationMarks() + "\n\n" + tmc.getStructuralMarks());
				System.out.println(sb.toString());
				e.printStackTrace();
			} finally {
				long stop = System.currentTimeMillis();
				if ((stop - start) + 1 < options.valueOf(timeoutOption) * 1000) {
					sb.append(String.format(", %d\n", (stop - start) + 1));
				}
				else {
					sb.append(String.format(", %d\n", 0));
				}
				writer.printf(sb.toString());
				executor.shutdownNow();
				mexecutor.shutdownNow();
				texecutor.shutdownNow();
			}
		}

		writer.close();
		positiveRecord.close();
		truePositiveRecord.close();
	}
}
