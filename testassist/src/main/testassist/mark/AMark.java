package testassist.mark;

public class AMark extends PoisonMark {
	
	public AMark(String content) {
		super(content);
	}

	@Override
	public String toString(){
		return String.format("App init : %s", super.toString());
	}

}
