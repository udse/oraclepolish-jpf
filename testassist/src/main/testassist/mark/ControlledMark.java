package testassist.mark;

public class ControlledMark extends DetoxMark {

	public ControlledMark(String content) {
		super(content);
	}

	@Override
	public String toString(){
		return String.format("Controlled Input : %s", super.toString());
	}
}
