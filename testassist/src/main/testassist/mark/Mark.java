package testassist.mark;

import com.google.common.base.Objects;

public abstract class Mark {

	protected String content;


	public Mark(final String name) {
		this.content = name;
	}
	
	@Override
	public String toString() {
		return String.format("::<%s>::", content);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(content);
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj instanceof Mark) {
			final Mark other = (Mark) obj;
			return Objects.equal(content, other.content);
		} else {
			return false;
		}
	}

}
