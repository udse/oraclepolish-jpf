package testassist.mark;

import edu.udel.testassist.configuration.TaintInfo;

public class SMark extends DetoxMark {

	public SMark(String content) {
		super(content);
	}

	@Override
	public String toString(){
		return String.format("Setup : %s", super.toString());
	}

}
