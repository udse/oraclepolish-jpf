package testassist.mark;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import testassist.util.Pair;

import com.google.common.base.Optional;
import com.google.common.base.Predicates;
import com.google.common.collect.ForwardingSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

public class Marks extends ForwardingSet<Mark> {

	final Set<Mark> delegate;

	public Marks() {
		super();
		delegate = new HashSet<>();
	}

	public Marks(Iterable<Marks> markss) {
		this();
		for (Marks marks : markss) {
			addAll(marks);
		}
	}

	public Marks(Mark... marks) {
		this();
		for (Mark mark : marks) {
			add(mark);
		}
	}

	public Marks(Object... marks) {
		this();

		if (marks != null)
			for (Object mark : marks) {
				if (mark instanceof Marks) addAll((Marks) mark);
			}
	}

	@SafeVarargs
	public static Marks union(Optional<Marks>... markss) {
		Iterable<Marks> marks = Optional.presentInstances(Arrays.asList(markss));
		if (Iterables.size(marks) > 0) {
			return new Marks(marks);
		}
		return null;
	}

	public static Marks union(Marks m1, Marks m2) {
		if (m1 != null) {
			m1.addMarks(m2);
			return m1;
		}
		else
			return m2;
	}

	public static Marks unionIter(Iterator<Pair<Integer, Marks>> iter) {
		Marks total = null;
		while (iter.hasNext())
			total = union(total, iter.next().getRight());

		return total;
	}

	public void addMarks(Marks ms) {
		if (ms != null)
			addAll(ms);
	}


	public int getSizeOfUncontrolledMarks() {
		return Sets.filter(delegate, Predicates.instanceOf(UncontrolledMark.class)).size();
	}

	public int getSizeOfControlledMarks() {
		return Sets.filter(delegate, Predicates.instanceOf(ControlledMark.class)).size();
	}

	public Marks getUncontrolledMarks() {
		Marks ms = new Marks();

		for (Mark m : delegate) {
			if (m instanceof UncontrolledMark) {
				ms.add(m);
			}
		}
		
		return ms;
	}

	public Marks getControlledMarks() {
		Marks ms = new Marks();

		for (Mark m : delegate) {
			if (m instanceof ControlledMark) {
				ms.add(m);
			}
		}
		
		return ms;
	}

	@Override
	protected Set<Mark> delegate() {
		return delegate;
	}

}
