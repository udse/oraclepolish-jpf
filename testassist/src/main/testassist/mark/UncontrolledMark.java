package testassist.mark;

public class UncontrolledMark extends PoisonMark {

	public UncontrolledMark(String content) {
		super(content);
	}

	@Override
	public String toString() {
		return String.format("Uncontrolled input : %s", super.toString());
	}

}
