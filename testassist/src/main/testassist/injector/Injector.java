package testassist.injector;

import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import testassist.mark.ControlledMark;
import testassist.mark.Marks;

public class Injector extends Object {

	public static Marks empty = new Marks(new ControlledMark("Nothing"));

	private String getClassName (StackFrame f) {
		return f.getMethodInfo().getClassInfo().getName();
	}

	public String generateTaintContent(StackFrame f, String content) {
		String location = String.format("%s.%s:%d", this.getClassName(f), f.getMethodName(),f.getPC().getPosition());
		return String.format("%s (%s)", content, location);
	}
	
	public Marks createControlledMarks(StackFrame f, String content) {
		return new Marks(new ControlledMark(generateTaintContent(f, content)));
	}

	public void taintTopOfOperandStack(StackFrame frame, String string) {
		// TODO Auto-generated method stub
		frame.setOperandAttr(createControlledMarks(frame, string));
	}

	public boolean inTest(StackFrame frame) {

		MethodInfo currentMethod = frame.getMethodInfo();
		String signature = currentMethod.getFullName();
		
		return signature.startsWith("test");
	}


}
