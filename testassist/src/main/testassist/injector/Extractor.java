package testassist.injector;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.MethodInfo;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.Types;
import testassist.mark.Marks;


import com.google.common.base.Optional;

public class Extractor {

	public Optional<Marks> getTaintFrom(StackFrame f, int offset) {
		return Optional.fromNullable(f.getOperandAttr(offset, Marks.class));
	}
	
	public Optional<Marks> getTaintFromTop(StackFrame f) {
		return getTaintFrom(f, 0);
	}
	
	public Optional<Marks> getTaints(StackFrame f, int size) {
		int i = 0;
		Optional<Marks> last = Optional.of(Injector.empty);
		while (size != i){
			last = Optional.of(Marks.union(last, getTaintFrom(f, i)));
			i = i + 1;
		}
		
		return last;
	}
}
