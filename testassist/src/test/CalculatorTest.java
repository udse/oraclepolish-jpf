import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import testassist.annotation.TestAssist;

public class CalculatorTest {

	private Calculator calc;

	@Before
	public void setUp() {
		calc = new Calculator();
	}

//	@Test
//	public void testAddInteger() {
//		TestAssist.startTest();
//		int i = TestAssist.taint(2);
//		int j = TestAssist.taint(3);
//		
//		TestAssist.startChecks();
//		assertEquals(5, calc.add(i, j));
//		TestAssist.endTest();
//	}
//	
	@Test
	public void testAddInteger() {
//		TestAssist.startTest();
		int i = 2;
		int j = 3;
		int k = i;
		
		TestAssist.startChecks();
		assertEquals(5, calc.add(k, j));
//		TestAssist.endTest();
	}
//	@Test
//	public void testAddLong() {
//		TestAssist.startTest();
//		long i = TestAssist.taint(2l);
//		long j = TestAssist.taint(3l);
//		
//		long result = calc.add(i, j);
//		TestAssist.startChecks();
//		assertEquals(5, result);
//		TestAssist.endTest();
//	}
//
//	@Test
//	public void testAddDouble() {
//		TestAssist.startTest();
//		double i = TestAssist.taint(2d);
//		double j = TestAssist.taint(3d);
//		
//		double result = calc.add(i, j);
//		TestAssist.startChecks();
//		assertEquals(5, result, 0);
//		TestAssist.endTest();
//	}

	
}
