public class Calculator {

	public int add(int... numbers) {
		
		int sum = 0;
		
		for (int number : numbers) {
			sum += number;
		}
		
		return sum;
	}
	
	public long add(long... numbers) {
		
		long sum = 0;
		
		for (long number : numbers) {
			sum += number;
		}
		
		return sum;
	}

	public double add(double... numbers) {
		
		double sum = 0;
		
		for (double number : numbers) {
			sum += number;
		}
		
		return sum;
	}

	
}
