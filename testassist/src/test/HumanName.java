public class HumanName {

	private final String first;
	private final String last;

	// private boolean cel;

	public HumanName(final String firstName, final String lastName) {
		this.last = lastName;
		this.first = firstName;
		// cel = true;
	}

	public HumanName(final String oneNameCelebrity) {
		this(null, oneNameCelebrity);
	}

	public String firstName() {
		return first;
	}

	public String lastName() {
		return last;
	}

	public boolean isCelebrity() {
		return null != last && null == first;
		// return cel;
	}
	
	public boolean stub(){
		boolean a = true, b=false;
		if (a){
			if (b){
				int i = 0;
			}
		}
		else {
			if (b){
				int i = 1;
			}
		}
		return true;
	}
}
