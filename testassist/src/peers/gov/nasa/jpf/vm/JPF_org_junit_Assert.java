package gov.nasa.jpf.vm;

import gov.nasa.jpf.annotation.MJI;

import com.google.common.base.Joiner;
import com.google.common.base.Objects;

public class JPF_org_junit_Assert extends NativePeer {

//	@MJI
//	public static void assertEquals__JJ__V(MJIEnv env, int clsref, long expected, long actual) {
//
//		StackFrame callerFrame = env.getCallerStackFrame();
//		MethodInfo method = callerFrame.getMethodInfo();
//
//		Object[] attributes = Objects.firstNonNull(env.getArgAttributes(), new Object[env.getMethodInfo().getNumberOfArguments()]);
//		System.out.printf("Assert : %s:%d - %s%s (%s)\n", callerFrame.getClassName(), callerFrame.getLine(), method.getName(), method.getSignature(),
//				Joiner.on(", ").skipNulls().join(attributes));
//	}
//
//	@MJI
//	public static void assertTrue__Z__V(MJIEnv env, int clsref, boolean expected) {
//
//		StackFrame callerFrame = env.getCallerStackFrame();
//		MethodInfo method = callerFrame.getMethodInfo();
//		Object[] attributes = Objects.firstNonNull(env.getArgAttributes(), new Object[env.getMethodInfo().getNumberOfArguments()]);
//		System.out.printf("Assert : %s:%d - %s%s (%s)\n", callerFrame.getClassName(), callerFrame.getLine(), method.getName(), method.getSignature(),
//				Joiner.on(", ").skipNulls().join(attributes));
//	}

//	@MJI
//	public void assertFalse__Ljava_lang_String_2Z__V(MJIEnv env, int clsObjRef, int rString0, boolean v1) {
//	}
//
//	@MJI
//	public void assertFalse__Z__V(MJIEnv env, int clsObjRef, boolean v0) {
//	}
//
//	@MJI
//	public void assertTrue__Ljava_lang_String_2Z__V(MJIEnv env, int clsObjRef, int rString0, boolean v1) {
//	}
//
//	@MJI
//	public void fail____V(MJIEnv env, int clsObjRef) {
//	}
//
//	@MJI
//	public void fail__Ljava_lang_String_2__V(MJIEnv env, int clsObjRef, int rString0) {
//	}
//
//	@MJI
//	public void assertEquals__DDD__V(MJIEnv env, int clsObjRef, double v0, double v1, double v2) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2DD__V(MJIEnv env, int clsObjRef, int rString0, double v1, double v2) {
//	}
//
//	@MJI
//	public void assertEquals__DD__V(MJIEnv env, int clsObjRef, double v0, double v1) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2JJ__V(MJIEnv env, int clsObjRef, int rString0, long v1, long v2) {
//	}
//
//	@MJI
//	public void assertEquals___3Ljava_lang_Object_2_3Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2_3Ljava_lang_Object_2_3Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertEquals__FFF__V(MJIEnv env, int clsObjRef, float v0, float v1, float v2) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2DDD__V(MJIEnv env, int clsObjRef, int rString0, double v1, double v2, double v3) {
//	}
//
//	@MJI
//	public void assertEquals__Ljava_lang_String_2FFF__V(MJIEnv env, int clsObjRef, int rString0, float v1, float v2, float v3) {
//	}
//
//	@MJI
//	public void assertNotEquals__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertNotEquals__Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void assertNotEquals__Ljava_lang_String_2JJ__V(MJIEnv env, int clsObjRef, int rString0, long v1, long v2) {
//	}
//
//	@MJI
//	public void assertNotEquals__DDD__V(MJIEnv env, int clsObjRef, double v0, double v1, double v2) {
//	}
//
//	@MJI
//	public void assertNotEquals__Ljava_lang_String_2DDD__V(MJIEnv env, int clsObjRef, int rString0, double v1, double v2, double v3) {
//	}
//
//	@MJI
//	public void assertNotEquals__JJ__V(MJIEnv env, int clsObjRef, long v0, long v1) {
//	}
//
//	@MJI
//	public void failEquals__Ljava_lang_String_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3F_3FF__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2, float v3) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3D_3DD__V(MJIEnv env, int clsObjRef, int r0, int r1, double v2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3F_3FF__V(MJIEnv env, int clsObjRef, int r0, int r1, float v2) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3C_3C__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3C_3C__V(MJIEnv env, int clsObjRef, int r0, int r1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3S_3S__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3S_3S__V(MJIEnv env, int clsObjRef, int r0, int r1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3B_3B__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3Ljava_lang_Object_2_3Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3Ljava_lang_Object_2_3Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0,
//			int rObject1, int rObject2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3B_3B__V(MJIEnv env, int clsObjRef, int r0, int r1) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3J_3J__V(MJIEnv env, int clsObjRef, int r0, int r1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3D_3DD__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2, double v3) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3J_3J__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2) {
//	}
//
//	@MJI
//	public void assertArrayEquals___3I_3I__V(MJIEnv env, int clsObjRef, int r0, int r1) {
//	}
//
//	@MJI
//	public void assertArrayEquals__Ljava_lang_String_2_3I_3I__V(MJIEnv env, int clsObjRef, int rString0, int r1, int r2) {
//	}
//
//	//	  @MJI
//	//	  public void internalArrayEquals__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V (MJIEnv env, int clsObjRef, int rString0, int rObject1, int rObject2) {
//	//	  }
//
//	@MJI
//	public void assertNotNull__Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0) {
//	}
//
//	@MJI
//	public void assertNotNull__Ljava_lang_String_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1) {
//	}
//
//	@MJI
//	public void assertNull__Ljava_lang_String_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1) {
//	}
//
//	@MJI
//	public void assertNull__Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0) {
//	}
//
//	@MJI
//	public void failNotNull__Ljava_lang_String_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1) {
//	}
//
//	@MJI
//	public void assertSame__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertSame__Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void assertNotSame__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertNotSame__Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rObject0, int rObject1) {
//	}
//
//	@MJI
//	public void failSame__Ljava_lang_String_2__V(MJIEnv env, int clsObjRef, int rString0) {
//	}
//
//	@MJI
//	public void failNotSame__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void failNotEquals__Ljava_lang_String_2Ljava_lang_Object_2Ljava_lang_Object_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rObject2) {
//	}
//
//	@MJI
//	public void assertThat__Ljava_lang_String_2Ljava_lang_Object_2Lorg_hamcrest_Matcher_2__V(MJIEnv env, int clsObjRef, int rString0, int rObject1,
//			int rMatcher2) {
//	}
//
//	@MJI
//	public void assertThat__Ljava_lang_Object_2Lorg_hamcrest_Matcher_2__V(MJIEnv env, int clsObjRef, int rObject0, int rMatcher1) {
//	}

}
