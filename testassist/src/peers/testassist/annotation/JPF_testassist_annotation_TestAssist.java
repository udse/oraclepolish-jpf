package testassist.annotation;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ElementInfo;
import gov.nasa.jpf.vm.FieldInfo;
import gov.nasa.jpf.vm.LocalVarInfo;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;
import gov.nasa.jpf.vm.StackFrame;
import gov.nasa.jpf.vm.StaticElementInfo;
import gov.nasa.jpf.vm.Types;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import testassist.mark.Marks;
import testassist.mark.ControlledMark;

import com.google.common.base.Joiner;
import com.google.common.base.Optional;
import com.google.common.collect.Iterables;

public class JPF_testassist_annotation_TestAssist extends NativePeer {

	@MJI
	public static double taint__D__D(MJIEnv env, int clsref, double value) {

		StackFrame frame = env.getCallerStackFrame();

		String location = String.format("%s:%d", frame.getMethodInfo().getClassInfo().getName(), frame.getLine());

		Marks marks = new Marks(new ControlledMark(String.format("%d (%s)", value, location)));

		System.out.printf("Tainted: %f with marks: (%s)\n", value, Joiner.on(", ").join(marks));

		env.setReturnAttribute(marks);

		return value;
	}

	@MJI
	public static float taint__F__F(MJIEnv env, int clsref, float value) {

		StackFrame frame = env.getCallerStackFrame();

		String location = String.format("%s:%d", frame.getMethodInfo().getClassInfo().getName(), frame.getLine());

		Marks marks = new Marks(new ControlledMark(String.format("%d (%s)", value, location)));

		System.out.printf("Tainted: %f with marks: (%s)\n", value, Joiner.on(", ").join(marks));

		env.setReturnAttribute(marks);

		return value;
	}

	@MJI
	public static long taint__J__J(MJIEnv env, int clsref, long value) {

		StackFrame frame = env.getCallerStackFrame();

		String location = String.format("%s:%d", frame.getMethodInfo().getClassInfo().getName(), frame.getLine());

		Marks marks = new Marks(new ControlledMark(String.format("%d (%s)", value, location)));

		System.out.printf("Tainted: %d with marks: (%s)\n", value, Joiner.on(", ").join(marks));

		env.setReturnAttribute(marks);
		return value;
	}

	@MJI
	public static int taint__I__I(MJIEnv env, int clsref, int value) {

		StackFrame frame = env.getCallerStackFrame();

		String location = String.format("%s:%d", frame.getMethodInfo().getClassInfo().getName(), frame.getLine());

		Marks marks = new Marks(new ControlledMark(String.format("%d (%s)", value, location)));

		System.out.printf("Tainted: %d with marks: (%s)\n", value, Joiner.on(", ").join(marks));

		env.setReturnAttribute(marks);
		return value;
	}

	@MJI
	public static int taint__Ljava_lang_String_2__Ljava_lang_String_2(MJIEnv env, int clsref, int objRef) {

		StackFrame frame = env.getCallerStackFrame();

		String location = String.format("%s:%d", frame.getMethodInfo().getClassInfo().getName(), frame.getLine());

		String value = env.getStringObject(objRef);

		Marks marks = new Marks(new ControlledMark(String.format("%s (%s)", value, location)));

		System.out.printf("Tainted: %s with marks: (%s)\n", value, Joiner.on(", ").join(marks));

		ElementInfo ei = env.getElementInfo(objRef);
		// ei.setObjectAttr(marks);

		for (FieldInfo field : ei.getClassInfo().getInstanceFields()) {
			ei.setFieldAttr(field, marks);
		}

		int valueRef = ei.getReferenceField("value");
		ElementInfo valueArray = env.getElementInfo(valueRef);

		// valueArray.setObjectAttr(marks);

		for (int i = 0; i < valueArray.arrayLength(); i++) {
			valueArray.setElementAttr(i, marks);
		}

		env.setReturnAttribute(marks);
		return objRef;
	}

	@MJI
	public static void startTest(MJIEnv env, int clsRef) {

	}

	@MJI
	public static void endTest(MJIEnv env, int clsRef) {

	}

	@MJI
	public static void startChecks____V(MJIEnv env, int clsRef) {

		System.out.println("\n\n-----Reachability------");
		
		StackFrame frame = env.getCallerStackFrame();

		LinkedList<ElementInfo> worklist = new LinkedList<>();

		Optional<Marks> attr = null;
		
		System.out.println("<ROOT>");
		
		for (LocalVarInfo local : frame.getLocalVars()) {

			System.out.printf("\t%s: ", local.getName());
			
			switch (Types.getBuiltinTypeFromSignature(local.getSignature())) {

				case Types.T_BOOLEAN: {
					boolean value = (1 == frame.getLocalVariable(local.getSlotIndex())) ? true : false;
					System.out.print(value);
					break;
				}
				case Types.T_BYTE: {
					byte value = (byte) frame.getLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_CHAR: {
					char value = (char) frame.getLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_DOUBLE: {
					double value = frame.getDoubleLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_FLOAT: {
					float value = frame.getFloatLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_INT: {
					int value = frame.getLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_LONG: {
					long value = frame.getLongLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_SHORT: {
					short value = (short) frame.getLocalVariable(local.getSlotIndex());
					System.out.print(value);
					break;
				}
				case Types.T_ARRAY:
				case Types.T_REFERENCE: {

					int ref = frame.getLocalVariable(local.getSlotIndex());

					ElementInfo obj = env.getElementInfo(ref);
					
					System.out.print(obj);
					
					if(null != obj) {
						worklist.addLast(obj);
					}

					break;
				}
			}
			
			attr = Optional.fromNullable(frame.getLocalAttr(local.getSlotIndex(), Marks.class));
			if(attr.isPresent()) {
				System.out.printf("Local :  [%s]", Joiner.on(", ").join(attr.get()));	
			}
			System.out.println();
		}
		System.out.println();

		Set<ElementInfo> visited = new HashSet<>();

		while (!worklist.isEmpty()) {
			ElementInfo obj = worklist.removeFirst();

			visited.add(obj);

			System.out.printf("%s", obj);
			
			attr = Optional.fromNullable(obj.getObjectAttr(Marks.class));
			if(attr.isPresent()) {
				System.out.printf(" [%s]", Joiner.on(", ").join(attr.get()));	
			}
			System.out.println();

			
			ClassInfo ci = obj.getClassInfo();
			StaticElementInfo sei = ci.getStaticElementInfo();

			Iterable<FieldInfo> fields = Iterables.concat(Arrays.asList(ci.getInstanceFields()), Arrays.asList(ci.getDeclaredStaticFields()));

			for (FieldInfo field : fields) {
				System.out.printf("\t%s: ", field.getName());

				switch (Types.getBuiltinTypeFromSignature(field.getSignature())) {

					case Types.T_BOOLEAN: {
						boolean value = field.isStatic() ? sei.getBooleanField(field) : obj.getBooleanField(field);
						System.out.print(value);
						break;
					}
					case Types.T_BYTE: {
						byte value = field.isStatic() ? sei.getByteField(field) : obj.getByteField(field);
						System.out.print(value);
						break;
					}
					case Types.T_CHAR: {
						char value = field.isStatic() ? sei.getCharField(field) : obj.getCharField(field);
						System.out.print(value);
						break;
					}
					case Types.T_DOUBLE: {
						double value = field.isStatic() ? sei.getDoubleField(field) : obj.getDoubleField(field);
						System.out.print(value);
						break;
					}
					case Types.T_FLOAT: {
						float value = field.isStatic() ? sei.getFloatField(field) : obj.getFloatField(field);
						System.out.print(value);
						break;
					}
					case Types.T_INT: {
						int value = field.isStatic() ? sei.getIntField(field) : obj.getIntField(field);
						System.out.print(value);
						break;
					}
					case Types.T_LONG: {
						long value = field.isStatic() ? sei.getLongField(field) : obj.getLongField(field);
						System.out.print(value);
						break;
					}
					case Types.T_SHORT: {
						short value = field.isStatic() ? sei.getShortField(field) : obj.getShortField(field);
						System.out.print(value);
						break;
					}
					case Types.T_ARRAY:
					case Types.T_REFERENCE: {
						int ref = field.isStatic() ? sei.getReferenceField(field) : obj.getReferenceField(field);

						ElementInfo value = env.getElementInfo(ref);

						System.out.print(value);

						if (null != value && !visited.contains(value)) {
							worklist.add(value);
						}

						break;
					}
				}

				attr = Optional.fromNullable(field.isStatic() ? sei.getFieldAttr(field, Marks.class) : obj.getFieldAttr(field, Marks.class));
				if(attr.isPresent()) {
					System.out.printf(" [%s]", Joiner.on(", ").join(attr.get()));	
				}
				System.out.println();
			}

			if (obj.isArray()) {
				for (int index = 0; index < obj.arrayLength(); index++) {

					System.out.printf("\t%d: ", index);

					switch (Types.getBuiltinTypeFromSignature(obj.getArrayType())) {

						case Types.T_BOOLEAN: {
							boolean value = obj.getBooleanElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_BYTE: {
							byte value = obj.getByteElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_CHAR: {
							char value = obj.getCharElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_DOUBLE: {
							double value = obj.getDoubleElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_FLOAT: {
							float value = obj.getFloatElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_INT: {
							int value = obj.getIntElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_LONG: {
							long value = obj.getLongElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_SHORT: {
							short value = obj.getShortElement(index);
							System.out.print(value);
							break;
						}
						case Types.T_ARRAY:
						case Types.T_REFERENCE: {

							int ref = obj.getReferenceElement(index);

							ElementInfo value = env.getElementInfo(ref);

							System.out.print(value);

							if (null != value && !visited.contains(value)) {
								worklist.add(value);
							}

							break;
						}
					}

					attr = Optional.fromNullable(obj.getElementAttr(index, Marks.class));
					if(attr.isPresent()) {
						System.out.printf(" [%s]", Joiner.on(", ").join(attr.get()));	
					}
					System.out.println();
				}
			}
			System.out.println();
		}
		System.out.println("-----------------------");
	}
}
